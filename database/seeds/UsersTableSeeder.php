<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	DB::table('users')->insert([

    		'fullname' => 'Administrator',
    		'username' => 'Admin',
    		'email' => 'm.ali8nl@gmail.com',
    		'address' => 'Netherlands',
    		'phone_number' => '+31 614959413',
    		'password' => bcrypt('Password'),
    		'role_id' => '1',
    	]);
           
    }
}
