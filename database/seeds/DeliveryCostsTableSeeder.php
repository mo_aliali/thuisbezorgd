<?php

use Illuminate\Database\Seeder;

class DeliveryCostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
       DB::table('delivery_costs')->insert(array(

           array(
               'id' => 1,
               'minimum_price' => '100',
               'maximum_price' => '4999',
               'delivery_type' => 0,
               'delivery_cost' => 500,
               "delivery_percentage" => null
              ),
           array(
               'id' => 2,
               'minimum_price' => '5000',
               'maximum_price' => '14999',
               'delivery_type' => 1,
               'delivery_percentage' => 10,
               'delivery_cost' => null
              ),
           array(
               'id' => 3,
               'minimum_price' => '15000',
               'maximum_price' => '49999',
               'delivery_type' => 1,
               'delivery_percentage' => 17,
               'delivery_cost' => null
              ),
           array(
               'id' => 4,
               'minimum_price' => '50000',
               'maximum_price' => '1000000',
               'delivery_type' => 1,
               'delivery_percentage' => 25,
               'delivery_cost' => null
              )
            ));
     }

}
