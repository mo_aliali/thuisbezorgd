<?php

use Illuminate\Database\Seeder;

class UnitTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
       DB::table('unit_types')->insert(array(

           array(
               'id' => 1,
               'name' => 'Gallon'
              ),

         array(
               'id' => 2,
               'name' => 'Tin',
              ),
        array(
               'id' => 3,
               'name' => 'Satchet',
              ),
        array(
               'id' => 4,
               'name' => 'Cup',
              ),
        array(
               'id' => 5,
               'name' => 'Carton',
              ),
        array(
               'id' => 6,
               'name' => 'Dozen',
              ),
        array(
               'id' => 7,
               'name' => 'Kilo',
              ),
        array(
               'id' => 8,
               'name' => 'Bowl',
              ),
        array(
               'id' => 9,
               'name' => 'Box',
              ),
        array(
               'id' => 10,
               'name' => 'Kilo',
              ),
         ));
     }

}
