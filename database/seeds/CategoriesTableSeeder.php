<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
     public function run()
     {
       DB::table('categories')->insert(array(

           array(
               'id' => 1,
               'name' => 'Food Stuffs'
              ),

         array(
               'id' => 2,
               'name' => 'Provisions',
              ),
         ));
     }

}
