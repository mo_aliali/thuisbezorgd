<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullname');
            $table->string('order_ref');
            $table->string('delivery_address');
            $table->string('city'); 
            $table->string('state'); 
            $table->string('mobile_1'); 
            $table->string('pay_method');
            $table->string('delivery_type');
            $table->integer('items');
            $table->decimal('sub_total', 10, 2);
            $table->decimal('delivery', 10, 2);
            $table->decimal('total', 10, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
