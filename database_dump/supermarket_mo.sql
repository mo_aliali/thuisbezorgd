-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 29, 2018 at 11:45 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `supermarket_mo`
--

-- --------------------------------------------------------

--
-- Table structure for table `billing_addresses`
--

CREATE TABLE `billing_addresses` (
  `id` int(10) UNSIGNED NOT NULL,
  `delivery_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Food Stuffs', NULL, NULL),
(2, 'Provisions', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `delivery_costs`
--

CREATE TABLE `delivery_costs` (
  `id` int(10) UNSIGNED NOT NULL,
  `minimum_price` decimal(10,2) NOT NULL,
  `maximum_price` decimal(10,2) NOT NULL,
  `delivery_type` tinyint(4) NOT NULL,
  `delivery_percentage` tinyint(4) DEFAULT NULL,
  `delivery_cost` decimal(10,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `delivery_costs`
--

INSERT INTO `delivery_costs` (`id`, `minimum_price`, `maximum_price`, `delivery_type`, `delivery_percentage`, `delivery_cost`, `created_at`, `updated_at`) VALUES
(5, '0.00', '1000.00', 0, NULL, '10.00', '2018-06-29 17:26:35', '2018-06-29 17:26:35');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_01_03_230550_create_roles_table', 1),
(2, '2014_10_12_000000_create_users_table', 1),
(3, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2017_03_10_161554_create_categories_table', 1),
(5, '2017_03_10_161615_create_products_table', 1),
(6, '2017_03_10_161638_create_unit_types_table', 1),
(7, '2017_03_10_161655_create_units_table', 1),
(8, '2017_07_12_113621_create_orders_table', 1),
(9, '2017_07_12_121541_create_billing_addresses_table', 1),
(10, '2017_07_12_145128_create_order_items_table', 1),
(11, '2017_07_13_161619_create_delivery_costs_table', 1),
(12, '2017_08_04_105623_add_perishable_to_products', 1),
(13, '2017_08_04_163607_add_user_id_to_billing_addresses', 1),
(14, '2017_08_04_163610_add_quantity_to_order_items', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order_ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `pay_method` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `delivery_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `items` int(11) NOT NULL,
  `sub_total` decimal(10,2) NOT NULL,
  `delivery` decimal(10,2) NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `fullname`, `order_ref`, `delivery_address`, `city`, `state`, `mobile_1`, `pay_method`, `delivery_type`, `items`, `sub_total`, `delivery`, `total`, `created_at`, `updated_at`) VALUES
(1, 'Mohammed Ali - Ali', 'tr_AwydeHFkgM', 'Grote Bickersstraat 311', 'Amsterdam', 'Amsterdam', '0614959413', 'paycard', 'Home Delivery', 1, '200.00', '10.00', '210.00', '2018-06-29 17:32:59', '2018-06-29 17:32:59'),
(2, 'Mohammed Ali - Ali', 'tr_3pEcnNnj5N', 'Grote Bickersstraat 311', 'Noord-Holland', 'Amsterdam', '0614959413', 'paycard', 'Home Delivery', 4, '180.00', '10.00', '190.00', '2018-06-29 17:34:54', '2018-06-29 17:34:54'),
(3, 'Mohammed Ali - Ali', 'tr_Hyh8e5TBaB', 'Grote Bickersstraat 311', 'Leiden', 'South-Holland', '0614959413', 'paycard', 'Home Delivery', 1, '50.00', '10.00', '60.00', '2018-06-29 17:45:28', '2018-06-29 17:45:28'),
(4, 'Mohammed Ali - Ali', 'tr_bzfyQDUg7j', 'Final', 'Final', 'North-Holland', '123', 'paycard', 'Home Delivery', 1, '100.00', '10.00', '110.00', '2018-06-29 19:37:15', '2018-06-29 19:37:15'),
(5, 'Mohammed Ali - Ali', 'tr_bRdDyFPN9D', 'FinalFinal', 'FinalFinal', 'North-Holland', '123', 'paycard', 'Home Delivery', 1, '20.00', '10.00', '30.00', '2018-06-29 19:43:35', '2018-06-29 19:43:35');

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `order_ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unit_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `order_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `quantity` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `order_items`
--

INSERT INTO `order_items` (`id`, `order_ref`, `unit_id`, `product_id`, `order_id`, `created_at`, `updated_at`, `quantity`) VALUES
(1, 'tr_AwydeHFkgM', 3, 2, 1, '2018-06-29 17:32:59', '2018-06-29 17:32:59', 2),
(2, 'tr_3pEcnNnj5N', 4, 3, 2, '2018-06-29 17:34:54', '2018-06-29 17:34:54', 1),
(3, 'tr_3pEcnNnj5N', 3, 2, 2, '2018-06-29 17:34:54', '2018-06-29 17:34:54', 1),
(4, 'tr_3pEcnNnj5N', 5, 4, 2, '2018-06-29 17:34:54', '2018-06-29 17:34:54', 1),
(5, 'tr_3pEcnNnj5N', 1, 1, 2, '2018-06-29 17:34:54', '2018-06-29 17:34:54', 1),
(6, 'tr_Hyh8e5TBaB', 1, 1, 3, '2018-06-29 17:45:28', '2018-06-29 17:45:28', 1),
(7, 'tr_bzfyQDUg7j', 3, 2, 4, '2018-06-29 19:37:15', '2018-06-29 19:37:15', 1),
(8, 'tr_bRdDyFPN9D', 4, 3, 5, '2018-06-29 19:43:35', '2018-06-29 19:43:35', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tags` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(75) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_ordered` int(11) NOT NULL DEFAULT '0',
  `is_featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `perishable` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `name`, `tags`, `category_id`, `description`, `image`, `no_ordered`, `is_featured`, `created_at`, `updated_at`, `perishable`) VALUES
(1, 'Taft Gell', 'Taft hair gell', 2, 'Taft power hair gell for man, waterproof. ', '/products/img/1530299277-71-BzAbtiHL._SL1500_.jpg', 0, 0, '2018-06-29 17:07:57', '2018-06-29 17:07:57', 0),
(2, 'Golden Apple', 'Golden Apple', 1, 'Golden Apple, 24k', '/products/img/1530299346-unnamed.png', 0, 1, '2018-06-29 17:09:06', '2018-06-29 17:09:13', 0),
(3, 'Banana', 'Banana', 1, 'Bananas from Spain', '/products/img/1530299481-banana_white_brush_77489_300x188.jpg', 0, 0, '2018-06-29 17:11:21', '2018-06-29 17:11:21', 0),
(4, 'Broccoli', 'Broccoli', 1, 'Broccoli from The Netherlands', '/products/img/1530299566-d-food-&-liquids-no-people.jpg', 0, 0, '2018-06-29 17:12:46', '2018-06-29 17:12:46', 0);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'content_admin', NULL, NULL),
(2, 'subscribers', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE `units` (
  `id` int(10) UNSIGNED NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `unit_type_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `price`, `unit_type_id`, `product_id`, `created_at`, `updated_at`) VALUES
(1, '50.00', 9, 1, '2018-06-29 17:07:57', '2018-06-29 17:07:57'),
(2, '40.00', 1, 1, '2018-06-29 17:07:57', '2018-06-29 17:07:57'),
(3, '100.00', 7, 2, '2018-06-29 17:09:06', '2018-06-29 17:09:06'),
(4, '20.00', 9, 3, '2018-06-29 17:11:21', '2018-06-29 17:11:21'),
(5, '10.00', 9, 4, '2018-06-29 17:12:46', '2018-06-29 17:12:46');

-- --------------------------------------------------------

--
-- Table structure for table `unit_types`
--

CREATE TABLE `unit_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `unit_types`
--

INSERT INTO `unit_types` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Gallon', NULL, NULL),
(2, 'Tin', NULL, NULL),
(3, 'Satchet', NULL, NULL),
(4, 'Cup', NULL, NULL),
(5, 'Carton', NULL, NULL),
(6, 'Dozen', NULL, NULL),
(7, 'Kilo', NULL, NULL),
(8, 'Bowl', NULL, NULL),
(9, 'Box', NULL, NULL),
(10, 'Kilo', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `fullname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` text COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fullname`, `username`, `email`, `address`, `phone_number`, `password`, `role_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'Admin', 'm.ali8nl@gmail.com', 'Netherlands', '+31 614959413', '$2y$10$TO2M6jZbWSLc5gRRVFY9BuFz3sa7PuHKbM82loqnDG2S9lkUCaBHi', 1, 'kqv1e6fk3msmNjfpwYiZAKNuU2Dgww5zGQiefOaqPCzWd0CLYzogfvqi2IJ3', NULL, '2018-06-29 17:31:23'),
(2, 'Mohammed Ali - Ali', 'mo_ali', 'mo@mogroupp.com', 'Grote Bickersstraat 311', '0612345678', '$2y$10$14ryY0wNOhG7x4pWMhyxxuCIKfHcSrDwKH4UlmWcz6GHn88YA8RHK', 2, 'qGbcda2ExkNRg5Evc9khVN5tcvIohnAKSrwbWuMoKYdNL0DoVsmFCwrFMTZX', '2018-06-29 17:00:56', '2018-06-29 19:37:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `billing_addresses`
--
ALTER TABLE `billing_addresses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `billing_addresses_user_id_foreign` (`user_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_costs`
--
ALTER TABLE `delivery_costs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_items_unit_id_foreign` (`unit_id`),
  ADD KEY `order_items_product_id_foreign` (`product_id`),
  ADD KEY `order_items_order_id_foreign` (`order_id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_category_id_foreign` (`category_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
  ADD PRIMARY KEY (`id`),
  ADD KEY `units_unit_type_id_foreign` (`unit_type_id`),
  ADD KEY `units_product_id_foreign` (`product_id`);

--
-- Indexes for table `unit_types`
--
ALTER TABLE `unit_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `billing_addresses`
--
ALTER TABLE `billing_addresses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `delivery_costs`
--
ALTER TABLE `delivery_costs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `unit_types`
--
ALTER TABLE `unit_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `billing_addresses`
--
ALTER TABLE `billing_addresses`
  ADD CONSTRAINT `billing_addresses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `order_items`
--
ALTER TABLE `order_items`
  ADD CONSTRAINT `order_items_order_id_foreign` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `order_items_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `order_items_unit_id_foreign` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);

--
-- Constraints for table `units`
--
ALTER TABLE `units`
  ADD CONSTRAINT `units_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `units_unit_type_id_foreign` FOREIGN KEY (`unit_type_id`) REFERENCES `unit_types` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
