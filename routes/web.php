<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

use App\products;

Route::group(['middleware' => ['auth']], function () {
    Route::get('ordered', array('uses' => 'AccountController@getOrdered', 'as' => 'getOrdered'));
});
Route::get('/', array('uses' => 'HomeController@index', 'as' => 'home'));

Route::get('login', array('uses' => 'AccountController@getLogin', 'as' => 'getLogin'));

Route::get('logout', array('uses' => 'AccountController@logout', 'as' => 'logout'));

Route::post('login', array('uses' => 'AccountController@postLogin', 'as' => 'postLogin'));

Route::get('register', array('uses' => 'AccountController@getRegister', 'as' => 'getRegister'));

Route::post('register', array('uses' => 'AccountController@postRegister', 'as' => 'postRegister'));

Route::get('forgot-password', array('uses' => 'AccountController@getForgotPassword', 'as' => 'getForgotPassword'));

Route::get('provision-items', array('uses' => 'ProvisionController@getProvision', 'as' => 'getProvision'));

Route::get('food-items', array('uses' => 'FoodController@getFoodItems', 'as' => 'getFoodItems'));

Route::get('featured-items', array('uses' => 'FeaturedController@getFeaturedItems', 'as' => 'getFeaturedItems'));

Route::get('contact', array('uses' => 'ContactController@getContact', 'as' => 'getContact'));

Route::get('faq', array('uses' => 'HomeController@getFaq', 'as' => 'getFaq'));

Route::get('delivery', array('uses' => 'HomeController@getDelivery', 'as' => 'getDelivery'));


Route::get('terms', array('uses' => 'HomeController@getTerms', 'as' => 'getTerms'));

Route::get('checkout', array('uses' => 'CheckoutController@getCheckout', 'as' => 'getCheckout'));

Route::get('pay-confirmation', array('uses' => 'CheckoutController@getPayConfirm', 'as' => 'getPayConfirm'));

Route::group(['middleware' => 'admin'], function () {

    Route::get('admin-dashboard', array('uses' => 'AdminController@adminDashboard', 'as' => 'adminDashboard'));

    Route::get('add-admin', array('uses' => 'AdminController@addAdmin', 'as' => 'addAdmin'));

    Route::get('feature-product/{id}', array('uses' => 'AdminController@featureProduct', 'as' => 'featureProduct'));

    Route::get('unfeature-product/{id}', array('uses' => 'AdminController@unfeatureProduct', 'as' => 'unfeatureProduct'));

    Route::get('admin', array('uses' => 'AdminController@addProduct', 'as' => 'addProduct'));

    Route::post('add-product', array('uses' => 'AdminController@storeProduct', 'as' => 'storeProduct'));

    Route::get('add-delivery', array('uses' => 'AdminController@addDelivery', 'as' => 'addDelivery'));
    Route::post('add-delivery', array('uses' => 'AdminController@addDeliveryCost', 'as' => 'addDeliveryCost'));

    Route::get('view-profile', array('uses' => 'AdminController@viewProfile', 'as' => 'viewProfile'));


    Route::get('admin-edit-profile', array('uses' => 'AdminController@adminEditProfile', 'as' => 'adminEditProfile'));

    Route::get('orders', array('uses' => 'AdminController@getOrders', 'as' => 'getOrders'));

});

Route::get('item/{id}', array('uses' => 'HomeController@getItem', 'as' => 'getitem'));

Route::get('item/{unit}/unit', array('uses' => 'HomeController@getUnit'));
Route::model('unit', 'App\Unit');

//Cart routes
Route::get('cart/add/{id}/{uid}', array('uses' => 'CartController@addCart'));
Route::get('cart/update/{id}/{quantity}', array('uses' => 'CartController@updateCart'));
Route::get('cart/remove/{id}', array('uses' => 'CartController@cartRemove'));

//Payment routes
Route::post('/pay', 'PaymentController@redirectToGateway')->name('pay');

Route::get('/payment/callback', 'PaymentController@handleGatewayCallback');

//password reset route
Route::post('password_reset', 'ResetPasswordController@reset')->name('password-resets');

Route::any('/search', function () {
    $q = Input::get('q');
    $user = products::where('name', 'LIKE', '%' . $q . '%')->orWhere('description', 'LIKE', '%' . $q . '%')->get();
    if (count($user) > 0)
        return view('welcome')->withDetails($user)->withQuery($q);
    else return view('welcome')->withMessage('No Details found. Try to search again !');
});

