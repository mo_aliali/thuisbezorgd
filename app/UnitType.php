<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitType extends Model
{
  protected $table = 'unit_types';
  public function units()
    {
        return $this->hasMany('App\Unit', 'product_id');
    }
}
