<?php
namespace App\Http\Controllers;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use View;
use App\Product;
class HomeController extends Controller
{
    /**
     * Display home page 
     
     * @return view
     */
    public function index()
    {
        $foodstuffs = Product::where('category_id', '=', 1)->simplePaginate(8);
        $provisions = Product::where('category_id', '=', 2)->simplePaginate(8);
        $featured = Product::orderBy('updated_at', 'Desc')->where('is_featured', '=', 1)->take(3)->get();
        return View::make('users.home')->with('foodstuffs', $foodstuffs)->with('provisions', $provisions)->with('featured', $featured);
    }
    /**
     * Display FAQ page.
     
     * @return view
     */
    public function getFaq()
    {
        return View::make('users.faq');
    }
    /**
     * Display page to show information about delivery
     
     * @return view
     */
    public function getDelivery()
    {
        return View::make('users.delivery');
    }
    /**
     * Display terms and conditions page
     
     * @return view
     */
    public function getTerms()
    {
        return View::make('users.terms');
    }
    /**
     * Display individual selected item
     
     * @return json
     */
    public function getUnit($unit)
    {
        return response()->json($unit->toArray());
    }
    public function getItem($id){
     $provision = Product::findOrFail($id);
        return view('users.item')->with('provision', $provision);
    }
}