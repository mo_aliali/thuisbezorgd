<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use View;

class FeaturedController extends Controller
{
	/**
     * Display featured items page
     
     * @return view
     */
    public function getFeaturedItems()
    {
    	return View::make('users.featured-items');
    }
}
