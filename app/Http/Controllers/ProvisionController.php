<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use View;
use App\Product;

class ProvisionController extends Controller
{
	/**
     * Display page to show items in the "provisions" category.
     
     * @return view
     */
    public function getProvision()
    {
        $provisions = Product::where('category_id', '=', 2)->get();
        return View::make('users.provision-items')->with('provisions', $provisions);
    }
}
