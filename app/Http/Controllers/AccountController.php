<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Auth;
use View;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Validator;
use App\User;



class AccountController extends Controller
{

	/**
     * Display the login page view

     * @return void
     */
	public function getLogin()
	{
		return View::make('accounts.login');
	}


	/**
     * Logs out a user from the application
     
     * @return void
     */
	public function logout()
	{
		Auth::logout();
		return redirect()->route('getLogin')->with('success', 'logged out, please login to continue');
	}



	/**
     * Display the register page.
     
     * @return view
     */
	public function getRegister()
	{
		return View::make('accounts.register');
	}
	


	/**
     * Post registration credentials to the database
     
     * @return void
     */
	public function postRegister(Request $request){
		$validate = Validator::make($request->all(), array(
			'full_name' => 'required|min:2|max:50',
			'username' => 'required|min:5|max:15|unique:users',
			'password' => 'required|min:6|max:25',
			'address' => 'required|min:2|max:40',
			'email' => 'required|email|min:2|max:40|unique:users',
			'confirm_password' => 'required|same:password',
			'phone_number' => 'required|digits:10',
		));
		if ($validate->fails()) {
			return back()->withErrors($validate)->withInput();
		} else {
			$user = new User();
			$user->fullname = $request->get('full_name');
			$user->username = $request->get('username');
			$user->email = $request->get('email');
			$user->address = $request->get('address');
			$user->password = bcrypt($request->get('password'));
			$user->phone_number = $request->get('phone_number');
			$user->role_id = $request->get('role_id');
			// $user->role_id = 1;
			if($user->save()){
				return redirect()->route('getLogin')->with('success', 'you registered sucessfully you can now login');
			} else {
				return back()->with('fail', 'an error occured while creating your profile')->withInput();
			}
		}
	}


	/**
     * Logs in a user 
     * @params username, password
     
     * @return void
     */
	public function postLogin(Request $request){
		$validate = Validator::make($request->all(), array(
			'username' => 'required|min:4',
			'password' => 'required|min:6',
		));
		if ($validate->fails()) {
			return back()->withErrors($validate)->withInput();
		} else {
			$auth = Auth::attempt(array(
				'username' => $request->get('username'),
				'password' => $request->get('password')
			));
			if ($auth) {
				return redirect()->intended('/')->with('success', 'you successfully logged in');
			}
			else{
				return back()->with('fail', 'invalid username or password')->withInput();
			}
		}
	}

	/**
     * Display checkout page 
     
     * @return view
     */
	public function getOrdered(){
		return redirect()->route('getCheckout');
	}

    
	/**
     * Display page for password reset
     
     * @return view
     */
	public function getForgotPassword()
	{
		return View::make('accounts.forgot-password');
	}
}
