<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Str;
use Auth;
class ResetPasswordController extends Controller
{
	

	public function __construct()
	{
		$this->middleware('guest');

	}
  

    
    public function reset(Request $request){
    	$user = User::where('email', '=', $request->input('email'));
    	if($user->first()){
    		$this->validate($request, [
    		'email' => 'required|email',
    		'password' => 'required|confirmed|min:6',
    		]);

    		$user->update([
            'password' => bcrypt($request->input('password')),
            'remember_token' => Str::random(60),
        ]);

        return redirect()->route('getLogin')->with('success', 'Password successfully changed please login with your new password');


    	}

    	else{
    		return redirect()->back()->with('fail', 'An error occured. we could not find your email.');
    	}

    }
}