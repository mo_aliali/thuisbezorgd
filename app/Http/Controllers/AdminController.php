<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use \Validator;
use View;
use App\User;
use App\Category;
use App\UnitType;
use App\Unit;
use App\Product;
use App\DeliveryCost;
use Image;
use App\OrderItem;
class AdminController extends Controller
{

  /**
     * Display the admin dashboard page view
     
     * @return view
     */
    public function adminDashboard()
    {
        return View::make('admin.admin-dashboard');
    }
    

    /**
     * Display the admin page to add delivery settings
     
     * @return view
     */
    public function addDelivery()
    {
        $deliveries = DeliveryCost::all();
        return View::make('admin.add-delivery')->with(compact('deliveries'));
    }

    /**
     * Display the admin page to add new products
     
     * @return view
     */

    public function addProduct()
    {
      $categories = Category::all();
      $unit_types = UnitType::all();
        return View::make('admin.add-product1')->with('categories', $categories)->with('unit_types', $unit_types);
    }


    /**
     * Store products added by admin into database
     
     * @return void
     */
    
    public function storeProduct(Request $request)
    {
      $validate = Validator::make($request->all(), array(
    'item_name' => 'required',
    'item_category' => 'required',
    'item_tag' => 'required',
    'item_details' => 'required',
    'item_image' => 'required',
));

    if($validate->fails())
    {
    return redirect()->back()->withErrors($validate)->withInput();
    }
    else
    {
      $product = new Product();
      $product->name = $request->get('item_name');
      $product->tags = $request->get('item_tag');
      $product->category_id = $request->get('item_category');
      $product->description = $request->get('item_details');
      $image = $request->file('item_image');
      $img = str_replace(' ', '-', $image->getClientOriginalName());
      $cool = strlen($img);
      if($cool > 40){
          $initial = $cool-30;
          $img = substr($img, $initial);
      }
              
      $imagename = time() . "-" . $img;
               
    Image::make($image->getRealPath())->resize(640, 480, function ($constraint) {
        $constraint->aspectRatio();})->save(public_path() . '/products/img/' . $imagename);
    $image_url = '/products/img/' .$imagename;
    $product->image = $image_url;
      if($product->save()){
        if($request->has('item_unit1') && $request->has('item_amount1')){
          $item = new Unit();
          $item->price = $request->get('item_amount1');
          $item->unit_type_id = $request->get('item_unit1');
          $item->product_id = $product->id;
          $item->save();
        }
        if($request->has('item_unit2') && $request->has('item_amount2')){
          $item = new Unit();
          $item->price = $request->get('item_amount2');
          $item->unit_type_id = $request->get('item_unit2');
          $item->product_id = $product->id;
          $item->save();
        }
        if($request->has('item_unit3') && $request->has('item_amount3')){
          $item = new Unit();
          $item->price = $request->get('item_amount3');
          $item->unit_type_id = $request->get('item_unit3');
          $item->product_id = $product->id;
          $item->save();
        }
        if($request->has('item_unit4') && $request->has('item_amount4')){
          $item = new Unit();
          $item->price = $request->get('item_amount4');
          $item->unit_type_id = $request->get('item_unit4');
          $item->product_id = $product->id;
          $item->save();
        }
        if($request->has('item_unit5') && $request->has('item_amount5')){
          $item = new Unit();
          $item->price = $request->get('item_amount5');
          $item->unit_type_id = $request->get('item_unit5');
          $item->product_id = $product->id;
          $item->save();
        }
        return redirect()->back()->with('success', 'you have successfully added the item');
      }
        return redirect()->back()->with('fail', 'an error occured while adding your item');
    }
}
    
    /**
     * Display page to add new admin
     
     * @return view
     */
    public function addAdmin()
    {
        return View::make('admin.add-admin');
    }

    /**
     * Display page to view profile
     
     * @return view
     */

    public function viewProfile()
    {
        return View::make('admin.view-profile');
    }

    /**
     * Display page to edit profile
     
     * @return view
     */
    public function adminEditProfile()
    {
      return View::make('admin.admin-edit-profile');
    }


    /**
     * Add item as featured product
     
     * @return void
     */
    public function featureProduct($id){
      $product = Product::find($id);
      $product->is_featured = 1;
      if($product->update()){
      return redirect()->back()->with('success', 'you have successfully featured the item');
      }
      return redirect()->back()->with('fail', 'an error occured while featuring the item');
    }
    
    /**
     * Remove item from featured product
     
     * @return void
     */
    public function unfeatureProduct($id){
      $product = Product::find($id);
      $product->is_featured = 0;
      if($product->update()){
      return redirect()->back()->with('success', 'you have successfully unfeatured the item');
      }
      return redirect()->back()->with('fail', 'an error occured while unfeaturing the item');
    }

    /**
     * Set delivery options and charges
     
     * @return void
     */
    public function addDeliveryCost(Request $request)
    {
      $validate = Validator::make($request->all(), array(
    'min_price' => 'required',
    'max_price' => 'required',
    'delivery_type' => 'required',
));

    if($validate->fails())
    {
    return redirect()->back()->withErrors($validate)->withInput();
    }
    else
    {
      if($request->get('delivery_type') == 0){
        $d_cost = new DeliveryCost();
        $d_cost->minimum_price = $request->get('min_price');
        $d_cost->maximum_price = $request->get('max_price');
        $d_cost->delivery_type = $request->get('delivery_type');
        $d_cost->delivery_cost = $request->get('delivery_cost');
        if($d_cost->save()){
          return redirect()->back()->with('success', 'you have successfully added the delivery cost');
        }
          return redirect()->back()->with('fail', 'an error occured while adding the delivery cost');
        }
      else{
        $d_cost = new DeliveryCost();
        $d_cost->minimum_price = $request->get('min_price');
        $d_cost->maximum_price = $request->get('max_price');
        $d_cost->delivery_type = $request->get('delivery_type');
        $d_cost->delivery_percentage = $request->get('delivery_percentage');
        if($d_cost->save()){
          return redirect()->back()->with('success', 'you have successfully added the delivery cost');
        }
          return redirect()->back()->with('fail', 'an error occured while adding the delivery cost');
        }
      }
    }
    public function getOrders(){
        $orders = OrderItem::all();
        return view("admin.orders")->with('orders', $orders);
    }
}