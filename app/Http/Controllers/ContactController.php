<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use View;

class ContactController extends Controller
{
	/**
     * Display contact page
     
     * @return view
     */
    public function getContact()
    {
    	return View::make('users.contact');
    }
}
