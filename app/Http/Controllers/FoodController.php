<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use View;
use App\Product;

class FoodController extends Controller
{
	/**
     * Display items of "foodstuff" category.
     
     * @return view
     */
    public function getFoodItems()
    {
        $foodstuffs = Product::where('category_id', '=', 1)->get();
        return View::make('users.food-items')->with('foodstuffs', $foodstuffs);
    }
}
