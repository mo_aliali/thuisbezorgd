<?php
namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use View;
use Session;

class CheckoutController extends Controller
{
  /**
     * Display page to checkout items in cart
     
     * @return view
     */
  public function getCheckout() {
    if(Session::has('order')){
      Session::forget('order');
    }
      return View::make('users.checkout');
  }

    /**
     * Display page to confirm payment
     
     * @return view
     */
  public function getPayConfirm() {
    if(Session::has('order')){
    $order = Session::get('order');
    return View::make('users.pay-confirmation')->with('order', $order);
    }
    return View::make('users.pay-confirmation');
  }
}
