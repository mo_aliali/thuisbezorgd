<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Paystack;
use App\Order;
use App\Unit;
use App\Product;
use App\BillingAddress;
use App\OrderItem;
use Session;
use Illuminate\Support\Facades\Auth;
use Cart;
use Mollie;
use RealRashid\SweetAlert\Facades\Alert;
class PaymentController extends Controller{
    /**
     * Redirect the User to Mollie Payment Page to make payment
     * @return Url
     */	    
    public function redirectToGateway(Request $request){
        
        $data = $request->all();

        $paymentMethod = $data['payment_method'];
        
        $payment = Mollie::api()->payments()->create([
        "amount"      => $data['total'],
        "description" => "Payment for purchases by ".$data['email'],
        "redirectUrl" => url('payment/callback'),
        ]);

        $data['payment_ID'] = $payment->id;

        Session::put('p_data', $data);

       $payment = Mollie::api()->payments()->get($payment->id);

        return redirect($payment->links->paymentUrl);
        
        }

    /**
     * Obtain Mollie payment information and submits the cart data
     *  to database if payment was successful

     * @return void
     */
    public function handleGatewayCallback(){

    $data = Session::get('p_data');
    foreach (Cart::content() as $item)
     $payment = Mollie::api()->payments()->get($data['payment_ID']);
    

    if ($payment->isPaid())
        {
            
            $order = new Order;
            $data['order_ref'] = $data['payment_ID'];
            $data['delivery_type'] = 'Home Delivery';
            $data['pay_method'] = $data['payment_method'];
            $data['fullname'] = Auth::user()->fullname;
            $order->fill($data);
            $order->save();
            foreach (Cart::content() as $item){
                $unit = Unit::find($item->options->unit);
                $product = Product::find($item->id);
                $order_item = new OrderItem;
                $order_item->order_id = $order->id;
                $order_item->order_ref = $data['order_ref'];
                $order_item->product_id = $product->id;
                $order_item->unit_id = $unit->id;
                $order_item->quantity = $item->qty;
                $order_item->save();
            }
            session::put('order', $order);
            Cart::destroy();
                return view('users.success');
        }else
        {
            return view('users.failed');
        }
        

    }
}