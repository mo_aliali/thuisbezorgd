<?php
namespace App\Http\Controllers;

use App\Product;
use App\Unit;
use Cart;

class CartController extends Controller
{
    /**
     * Add item to cart
     
     * @return json
     */
    public function addCart($id, $uid)
    {
        $product = Product::find($id);
        $unit = Unit::find($uid);
        Cart::associate('App\Product')->add(array('id' => $product->id, 'name' => $product->name, 'qty' => 1, 'price' => $unit->price, 'options' => ['image' => $product->image, 'unit' => $unit->id]));
        $res = ['cart'=> Cart::content(), 'count' => Cart::count()];
        return response()->json($res);
    }

    /**
     * Display page to view items in cart.
     
     * @return view
     */
    public function viewCart()
    {
        $products = Product::all();
        $categories = Category::all();
        return View::make('users.cart');
    }

    /**
     * Remove item from cart
     
     * @return json
     */

    public function cartRemove($row)
    {
        $rowId = $row;
        Cart::remove($rowId);
        $res = ['cart'=> Cart::content(), 'count' => Cart::count()];
        return response()->json($res);
    }

    /**
     * Update items in cart
     
     * @return json
     */
    public function updateCart($id, $quantity){
        Cart::update($id, $quantity);
        $res = ['cart'=> Cart::content(), 'count' => Cart::count()];
        return response()->json($res);
    }
}