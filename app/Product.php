<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';
    public function units()
    {
        return $this->hasMany('App\Unit', 'product_id');
    }
    public function dunit($id){
       $unit =  Unit::where('product_id', '=', $id)->first();
       return $unit;
    }   
}
