<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\DeliveryCost;
use App\Product;
use Cart;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   protected $guarded = [
       
   ];
    protected $table = 'users';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    public function delivery($total){
        $cost = DeliveryCost::where('minimum_price', '<=', $total)->where('maximum_price', '>=', $total)->first();
        $d_cost;
        if($cost->delivery_type == 0){
        $d_cost = $cost->delivery_cost;
        }
        else{
            $d_cost = $cost->delivery_percentage * $total * 0.01;
        }
        return $d_cost;
    }
    public function billing()
    {
        return $this->hasOne('App\BillingAddress', 'user_id');
    }
    public function perishable() {
        foreach (Cart::content() as $item){
        $product = Product::find($item->id);
        if($product->perishable == 1){
            return true;
            }
        }
        return false;
    }  
}
