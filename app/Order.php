<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';
    protected $fillable = [
        'order_ref','fullname', 'delivery_address', 'state', 'city', 'delivery_type', 'items', 'sub_total', 'delivery', 'total', 'mobile_1', 'mobile_2','pay_method'
    ];
}
