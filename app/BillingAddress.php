<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillingAddress extends Model
{
    //
    protected $table = 'billing_addresses';
    protected $fillable = [
        'delivery_address', 'city', 'state', 'mobile_1', 'mobile_2'
    ];
}
