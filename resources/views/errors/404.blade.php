<!DOCTYPE html>
<html>
    <head>
        <title>404</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
                font-family: 'Lato', sans-serif;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="text-center">
                    <img class="img-responsive" width="450px;" src="{{URL::asset('images/404.gif')}}">
                </div>
                <div class="title">Page Not Found.</div>
                    <p class="lead">You seem to have lost your way. Kindly go back <a href="{{route('home')}}" class="btn btn-primary">Home</a></p>
            </div>
        </div>
    </body>
</html>
