@extends('layouts.front.adminMaster')
@section('body')

    <!-- Banner -->
    <div class="banner-top">
        <div class="container">
            <h3>Add an store</h3>
            <h4><a href="{{ url('/')}}">Home</a><label>/</label>Add an store</h4>
            <div class="clearfix"></div>
        </div>
    </div>

    <!-- Second Content -->
    <div class="product">
        <div class="container">

            <div class="spec">
                <h3>Add an Store</h3>
                <div class="ser-t">
                    <b></b>
                    <span><i></i></span>
                    <b class="line"></b>
                </div>
            </div>

            @if(Session::has('success'))
                <div class="container">
                    <div class="alert alert-success">
                        {{Session::get('success')}}
                    </div>
                </div>
            @elseif(Session::has('fail'))
                <div class="container">
                    <div class="alert alert-danger">
                        {{Session::get('fail')}}
                    </div>
                </div>
        @endif

        <!-- Register -->

            <div class="login">
                <div class="auth-container">
                    <h3>Fill in store Details</h3>
                    <form action="{{ URL::route('postRegister')}}" method="post" enctype="multipart/form-data">
                        <div class="authInput-container {{ ($errors->has('full_name')) ? 'has-error' : ''}}">
                            <label>Full Name</label>
                            <input type="text" placeholder="Enter Full Name" name="full_name" required=""
                                   class="input-field">
                        </div>
                        @if ($errors->has('full_name'))
                            <span style="color: palevioletred; margin-top: 10%;">{{ $errors->first('full_name') }}</span>
                        @endif

                        <div class="authInput-container {{ ($errors->has('username')) ? 'has-error' : ''}}">
                            <label>User Name</label>
                            <input type="text" placeholder="Enter User Name" name="username" required=""
                                   class="input-field">
                        </div>
                        @if ($errors->has('username'))
                            <span style="color: palevioletred; margin-top: 10%;">{{ $errors->first('username') }}</span>
                        @endif

                        <div class="authInput-container {{ ($errors->has('email')) ? 'has-error' : ''}}">
                            <label>Email</label>
                            <input type="email" placeholder="Enter Email" name="email" required="" class="input-field">
                        </div>
                        @if ($errors->has('email'))
                            <span style="color: palevioletred; margin-top: 10%;">{{ $errors->first('email') }}</span>
                        @endif

                        <div class="authInput-container {{ ($errors->has('password')) ? 'has-error' : ''}}">
                            <label>Password</label>
                            <input type="password" placeholder="Enter Password" name="password" required=""
                                   class="input-field">
                        </div>
                        @if ($errors->has('password'))
                            <span style="color: palevioletred; margin-top: 10%;">{{ $errors->first('password') }}</span>
                        @endif

                        <div class="authInput-container {{ ($errors->has('confirm_password')) ? 'has-error' : ''}}">
                            <label>Confirm Password</label>
                            <input type="password" placeholder="Confirm Password" name="confirm_password" required=""
                                   class="input-field">
                        </div>
                        @if ($errors->has('confirm_password'))
                            <span style="color: palevioletred; margin-top: 10%;">
                            {{ $errors->first('confirm_password') }}
                        </span>
                        @endif

                        <div class="authInput-container {{ ($errors->has('phone_number')) ? 'has-error' : ''}}">
                            <label>Phone Number</label>
                            <input type="number" placeholder="Enter Phone Number" name="phone_number" required=""
                                   class="input-field">
                        </div>
                        @if ($errors->has('phone_number'))
                            <span style="color: palevioletred; margin-top: 10%;">
                        {{ $errors->first('phone_number') }}
                    </span>
                        @endif

                        <div class="authInput-container {{ ($errors->has('address')) ? 'has-error' : ''}}">
                            <label>Address</label>
                            <input type="text" placeholder="Enter Address" name="address" required=""
                                   class="input-field">
                        </div>
                        @if ($errors->has('address'))
                            <span style="color: palevioletred; margin-top: 10%;">
                            {{ $errors->first('address') }}
                        </span>
                        @endif

                        <div class="forgot-password"></div>

                        <div class="forgotBtn-container">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="role_id" value="1">
                            <input type="submit" value="Add Admin" class="forgotBtn">
                        </div>


                    </form>

                </div>
            </div>


        <!-- <div class="main-agileits">
                    <div class="form-w3agile">
                        <form action="{{ URL::route('postRegister')}}" method="post" enctype="multipart/form-data">
                            <div class="key {{ ($errors->has('full_name')) ? 'has-error' : ''}}">
                                <i class="fa fa-user" aria-hidden="true"></i>
                                <input type="text" value="{{old('full_name') ? old('full_name') : 'fullname'}}" placeholder="full name"  name="full_name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Full Name';}" required="">
                                <div class="clearfix"></div>
                                @if ($errors->has('full_name'))
            <span style="color: palevioletred; margin-top: 10%;">{{ $errors->first('full_name') }}</span>
                                @endif
                </div>
                <div class="key {{ ($errors->has('username')) ? 'has-error' : ''}}">
                                <i class="fa fa-users" aria-hidden="true"></i>
                                <input type="text" value="{{old('username') ? old('username') : 'username'}}" placeholder="username"  name="username" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Username';}" required="">
                                <div class="clearfix"></div>
                                @if ($errors->has('username'))
            <span style="color: palevioletred; margin-top: 10%;">{{ $errors->first('username') }}</span>
                                @endif
                </div>
                <div class="key {{ ($errors->has('email')) ? 'has-error' : ''}}">
                                <i class="fa fa-envelope" aria-hidden="true"></i>
                                <input type="text" value="{{old('email') ? old('email') : 'email'}}" placeholder="email"  name="email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
                                <div class="clearfix"></div>
                                @if ($errors->has('email'))
            <span style="color: palevioletred; margin-top: 10%;">{{ $errors->first('email') }}</span>
                                @endif
                </div>
                <div class="key {{ ($errors->has('password')) ? 'has-error' : ''}}">
                                <i class="fa fa-lock" aria-hidden="true"></i>
                                <input type="password" value="{{old('password') ? old('password') : 'password'}}" placeholder="password"  name="password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required="">
                                <div class="clearfix"></div>
                                @if ($errors->has('password'))
            <span style="color: palevioletred; margin-top: 10%;">{{ $errors->first('password') }}</span>
                                @endif
                </div>
                <div class="key {{ ($errors->has('confirm_password')) ? 'has-error' : ''}}">
                                <i class="fa fa-lock" aria-hidden="true"></i>
                                <input type="password" value="{{old('confirm_password') ? old('confirm_password') : 'confirm password'}}" placeholder="confirm password"  name="confirm_password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Confirm Password';}" required="">
                                <div class="clearfix"></div>
                                @if ($errors->has('confirm_password'))
            <span style="color: palevioletred; margin-top: 10%;">{{ $errors->first('confirm_password') }}</span>
                                @endif
                </div>
                <div class="key {{ ($errors->has('phone_number')) ? 'has-error' : ''}}">
                                <i class="fa fa-mobile-phone" aria-hidden="true"></i>
                                <input type="text" value="{{old('phone_number') ? old('phone_number') : 'phone number'}}" placeholder="phone number"  name="phone_number" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Phone Number';}" required="">
                                <div class="clearfix"></div>
                                @if ($errors->has('phone_number'))
            <span style="color: palevioletred; margin-top: 10%;">{{ $errors->first('phone_number') }}</span>
                                @endif
                </div>
                <div class="key {{ ($errors->has('address')) ? 'has-error' : ''}}">
                                <i class="fa fa-home" aria-hidden="true"></i>
                                <input type="text" value="{{old('address') ? old('address') : 'address'}} " placeholder="address"  name="address" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Address'}" required="">
                                <div class="clearfix"></div>
                                @if ($errors->has('address'))
            <span style="color: palevioletred; margin-top: 10%;">{{ $errors->first('address') }}</span>
                                @endif
                </div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="role_id" value="1">
                            <input type="submit" value="Add Account">
                        </form>
                    </div>
                    <div class="forg">
                        <div class="clearfix"></div>
                    </div>
                </div> -->
            <!-- /.Register -->
        </div>

    </div>

@stop
