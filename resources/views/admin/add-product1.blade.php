@extends('layouts.front.adminMaster')
@section('body')

    <!-- Banner -->
    <div class="banner-top">
        <div class="container">
            <h3>Add Store</h3>
            <h4><a href="{{ url('/')}}">Home</a><label>/</label>Add Store</h4>
            <div class="clearfix"></div>
        </div>
    </div>

    <!-- Second Content -->
    <div class="product">
        <div class="container">
            <div class="spec">
                <h3>Add Store</h3>
                <div class="ser-t">
                    <b></b>
                    <span><i></i></span>
                    <b class="line"></b>
                </div>
            </div>

            <div class="login">
                <div class="auth-container">

                    @if(Session::has('success'))
                        <div class="container">
                            <div class="alert alert-success">
                                {{Session::get('success')}}
                            </div>
                        </div>
                    @elseif(Session::has('fail'))
                        <div class="container">
                            <div class="alert alert-danger">
                                {{Session::get('fail')}}
                            </div>
                        </div>
                    @endif

                    <h3>Fill in Item Details</h3>
                    <h5>Item Info</h5>
                    <form action="{{URL::route('storeProduct')}}" method="post" enctype="multipart/form-data">
                        <div class="authInput-container {{ ($errors->has('item_name')) ? 'has-error' : ''}}">
                            <label>Item Name</label>
                            <input type="text" placeholder="Name of Item" name="item_name" required=""
                                   class="input-field">
                        </div>
                        @if ($errors->has('item_name'))
                            <span style="color: palevioletred; margin-top: 10%;">{{ $error->first('item_name') }}</span>
                        @endif

                        <div class="authInput-container {{ ($errors->has('item_details')) ? 'has-error' : ''}}">
                            <label>Item Details</label>
                            <input type="text" placeholder="Item Details" name="item_details" required=""
                                   class="input-field">
                        </div>
                        @if ($errors->has('item_details'))
                            <span style="color: palevioletred; margin-top: 10%;">{{ $error->first('item_details') }}</span>
                        @endif

                        <div class="authInput-container {{ ($errors->has('item_details')) ? 'has-error' : ''}}">
                            <label>Item Tag</label>
                            <input type="text" placeholder="Item Tag" name="item_tag" required="" class="input-field">
                        </div>
                        @if ($errors->has('item_tag'))
                            <span style="color: palevioletred; margin-top: 10%;">{{ $error->first('item_tag') }}</span>
                        @endif

                        <div class="authInput-container {{ ($errors->has('item_catego')) ? 'has-error' : ''}}">
                            <label>Item Category</label>
                            <select id="item_category" name="item_category" style="width: 325px;">
                                <option>Select Category</option>
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                                <option value="Other">Other, please specify</option>
                            </select>
                        </div>

                        <div class="authInput-container">
                            <input type="checkbox" name="perishable" value="perishable" id="perishable"/>
                            <label for="perishable">Perishable</label>
                        </div>

                        <div class="authInput-container">
                            <label for="item_image">Item Image:</label>
                            <label class="btn btn-default btn-file" style="width: 325px;">
                                Select File
                                <input type="file" style="display:none;" name="item_image">
                            </label>
                            <span class='label label-info' id="upload-file-info"></span>
                        </div>

                        <h5>Item Units</h5>
                        <div class="authInput-container">
                            <label for="item_unit1">Item Unit 1:</label>
                            <select name="item_unit1" id="item_unit"
                                    onchange="showfield(this.options[this.selectedIndex].value)" style="width: 325px;">
                                <option>Select unit type</option>
                                @foreach($unit_types as $unit_type)
                                    <option value="{{$unit_type->id}}">{{$unit_type->name}}</option>
                                @endforeach
                            </select>
                            <br><br>
                            <input type="number" name="item_amount1" placeholder="&#8364; 20"
                                   class="iUnit form-control">
                        </div>

                        <div class="authInput-container">
                            <label for="item_unit2">Item Unit 2:</label>
                            <select name="item_unit2" id="item_unit"
                                    onchange="showfield(this.options[this.selectedIndex].value)" style="width: 325px;">
                                <option>Select unit type</option>
                                @foreach($unit_types as $unit_type)
                                    <option value="{{$unit_type->id}}">{{$unit_type->name}}</option>
                                @endforeach
                            </select>
                            <br><br>
                            <input type="number" name="item_amount2" placeholder="&#8364; 20"
                                   class="iUnit form-control">
                        </div>

                        <div class="authInput-container">
                            <label for="item_unit3">Item Unit 3:</label>
                            <select name="item_unit3" id="item_unit"
                                    onchange="showfield(this.options[this.selectedIndex].value)" style="width: 325px;">
                                <option>Select unit type</option>
                                @foreach($unit_types as $unit_type)
                                    <option value="{{$unit_type->id}}">{{$unit_type->name}}</option>
                                @endforeach
                            </select>
                            <br><br>
                            <input type="number" name="item_amount3" placeholder="&#8364; 20"
                                   class="iUnit form-control">
                        </div>

                        <div class="authInput-container">
                            <label for="item_unit3">Item Unit 4:</label>
                            <select name="item_unit4" id="item_unit"
                                    onchange="showfield(this.options[this.selectedIndex].value)" style="width: 325px;">
                                <option>Select unit type</option>
                                @foreach($unit_types as $unit_type)
                                    <option value="{{$unit_type->id}}">{{$unit_type->name}}</option>
                                @endforeach
                            </select>
                            <br><br>
                            <input type="number" name="item_amount4" placeholder="&#8364; 20"
                                   class="iUnit form-control">
                        </div>

                        <div class="authInput-container">
                            <label for="item_unit3">Item Unit 5:</label>
                            <select name="item_unit5" id="item_unit"
                                    onchange="showfield(this.options[this.selectedIndex].value)" style="width: 325px;">
                                <option>Select unit type</option>
                                @foreach($unit_types as $unit_type)
                                    <option value="{{$unit_type->id}}">{{$unit_type->name}}</option>
                                @endforeach
                            </select>
                            <br><br>
                            <input type="number" name="item_amount5" placeholder="&#8364; 20"
                                   class="iUnit form-control">
                        </div>

                        <div class="forgot-password"></div>

                        <div class="forgotBtn-container">
                            <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}"/>
                            <input type="submit" value="Add Store" class="forgotBtn">
                        </div>

                    </form>
                </div>
            </div>

        <!--
            <div class="add-product">
                <div class="main-addProduct">
                    <div class="form-style-5">
                        <form action="{{URL::route('storeProduct')}}" method="post" enctype="multipart/form-data">
                            <fieldset>
                                <legend><span class="number">1</span>Item Info</legend>
                                <div {{($errors->has('item_name')) ? 'has-error' : ''}}>
                                    <input type="text" name="item_name" placeholder="Item Name *" required>
                                    @if($errors->has('item_name'))
            {{$errors->first('item_name')}}
        @endif
                </div>

                <div {{($errors->has('item_details')) ? 'has-error' : ''}}>
                                    <textarea name="item_details" placeholder="Item Details *" id="" cols="30" rows="10" required></textarea>
                                    @if($errors->has('item_details'))
            {{$errors->first('item_details')}}
        @endif
                </div>

                <div {{($errors->has('item_tag')) ? 'has-error' : ''}}>
                                    <input type="text" name="item_tag" placeholder="Item Tag *" required>
                                    @if($errors->has('item_tag'))
            {{$errors->first('item_tag')}}
        @endif
                </div>

                <label for="item_category">Item Category:</label>
                <br><br>
                <div {{ ($errors->has('item_catego')) ? 'has-error' : ''}} >
                                    <select id="item_category" name="item_category" style="width: 325px;">
                                        <option value="">Select Category</option>
                                        @foreach($categories as $category)
            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                <option value="Other">Other, please specify</option>
            </select>
        </div>

        <div style="position: relative;">
            <input type="checkbox" name="perishable" value="perishable" id="perishable" style="position: relative; margin-left: -150px; display: inline;">
            <label for="perishable" style="display: relative; bottom: -12px; left: 20px;">Perishable?</label>
        </div>

        <label for="item_image">Item Image:</label>
        <br><br>
        <label class="btn btn-default btn-file" style="width: 325px;">
            Select File
            <input type="file" style="display:none;" name="item_image">
        </label>
        <span class='label label-info' id="upload-file-info"></span>

    </fieldset>

    <fieldset style="margin-top: 50px;">
        <legend><span class="number">2</span>Item Units</legend>

        <label for="item_unit1">Item Unit 1:</label>
        <br><br>
        <select name="item_unit1" id="item_unit" onchange="showfield(this.options[this.selectedIndex].value)" style="width: 325px;">
                                    @foreach($unit_types as $unit_type)
            <option value="{{$unit_type->id}}">{{$unit_type->name}}</option>
                                    @endforeach
                </select>

                <input type="number" name="item_amount1" placeholder="Item Amount">

                <label for="item_unit1">Item Unit 2:</label>
                <br><br><br>
                <select name="item_unit2" id="item_unit" onchange="showfield(this.options[this.selectedIndex].value)" style="width: 325px;">
                                    @foreach($unit_types as $unit_type)
            <option value="{{$unit_type->id}}">{{$unit_type->name}}</option>
                                    @endforeach
                </select>

                <input type="number" name="item_amount2" placeholder="Item Amount">

                <label for="item_unit3">Item Unit 3:</label>
                <br><br><br>
                <select name="item_unit3" id="item_unit" onchange="showfield(this.options[this.selectedIndex].value)" style="width: 325px;">
                                    @foreach($unit_types as $unit_type)
            <option value="{{$unit_type->id}}">{{$unit_type->name}}</option>
                                    @endforeach
                </select>

                <input type="number" name="item_amount3" placeholder="Item Amount">

                <label for="item_unit4">Item Unit 4:</label>
                <br><br><br>
                <select name="item_unit4" id="item_unit" onchange="showfield(this.options[this.selectedIndex].value)" style="width: 325px;">
                                    @foreach($unit_types as $unit_type)
            <option value="{{$unit_type->id}}">{{$unit_type->name}}</option>
                                    @endforeach
                </select>

                <input type="number" name="item_amount4" placeholder="Item Amount">

                <label for="item_unit5">Item Unit 5:</label>
                <br><br><br>
                <select name="item_unit5" id="item_unit" onchange="showfield(this.options[this.selectedIndex].value)" style="width: 325px;">
                                    @foreach($unit_types as $unit_type)
            <option value="{{$unit_type->id}}">{{$unit_type->name}}</option>
                                    @endforeach
                </select>

                <input type="number" name="item_amount5" placeholder="Item Amount">

            </fieldset>

            <button type="submit" class="payment" style="background-color: #1ABC9C; border: none;">Add Product</button>
            <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                        </form>
                    </div>
                </div>
            </div>
-->
        </div>
    </div>
    <!-- /.Second Content -->


@stop
