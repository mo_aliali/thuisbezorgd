@extends('layouts.front.adminMaster')
@section('body')

    <!-- Banner -->
    <div class="banner-top">
        <div class="container">
            <h3>Add Delivery Charge</h3>
            <h4><a href="{{ url('/')}}">Home</a><label>/</label>Add Delivery Charge</h4>
            <div class="clearfix"></div>
        </div>
    </div>

    <!-- Second Content -->
    <div class="product">
        <div class="container">

            <div class="spec">
                <h3>Add Delivery Charge</h3>
                <div class="ser-t">
                    <b></b>
                    <span><i></i></span>
                    <b class="line"></b>
                </div>
            </div>

            <div class="login">
                <div class="auth-container">
                    @if (Session::has('success'))
                        <div class="container">
                            <div class="alert alert-success"> {{ Session::get('success') }}</div>
                        </div>
                    @elseif (Session::has('fail'))
                        <div class="container">
                            <div class="alert alert-danger"> {{ Session::get('fail') }}</div>
                        </div>
                    @endif

                    <h3>Fill in Delivery Range</h3>
                    <form action="{{URL::route('addDeliveryCost')}}" method="post" enctype="multipart/form-data">
                        <div class="authInput-container">
                            <label for="price_details">Price Details</label>
                            <input type="number" name="max_price" placeholder="Maximum Price *" class="form-control"
                                   required>
                            <br>
                            <input type="number" name="min_price" placeholder="Minimum Price *" class="form-control"
                                   required>
                        </div>

                        <div class="authInput-container">
                            <label for="delivery_type">Charge Type</label>
                            <select id="delivery_type" onchange="dType(this.value)" name="delivery_type"
                                    style="width: 325px;">
                                <option value="">Select Type</option>
                                <option value="0">Flat</option>
                                <option value="1">Percentage</option>
                            </select>
                            <br><br>
                            <div id="flat">
                                <input type="number" name="delivery_cost" placeholder="&#8364; Delivery Cost *"
                                       class="form-control">
                            </div>
                            <br>
                            <div id="percentage">
                                <input type="number" name="delivery_percentage"
                                       placeholder="&#8364; Delivery Percentage *" class="form-control">
                            </div>
                        </div>

                        <div class="forgot-password"></div>

                        <div class="forgotBtn-container">
                            <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}">
                            <input type="submit" value="Add Delivery Range" class="forgotBtn">
                        </div>
                    </form>
                </div>
            </div>

        <!--
        <div class="add-product">
            <div class="main-addProduct">
            @if (Session::has('success'))
            <div class="container">
                <div class="alert alert-success"> {{ Session::get('success') }}</div>
                </div>
            @elseif (Session::has('fail'))
            <div class="container">
                <div class="alert alert-danger"> {{ Session::get('fail') }}</div>
                </div>
            @endif
                <div class="form-style-5">
                    <form action="{{URL::route('addDeliveryCost')}}" method="post" enctype="multipart/form-data">
                        <fieldset>
                            <legend>Price Detail</legend>
                            
                            <div>
                                <input type="text" name="max_price" placeholder="Maximum Price *" required>
                            </div>
                            
                            <div>
                                <input type="text" name="min_price" placeholder="Minimum Price *" required>
                            </div>
                            
                            <label for="delivery_type">Select Delivery Type</label>
                            <br><br>
                            <div>
                                <select id="delivery_type" onchange="dType(this.value)" name="delivery_type" style="width: 325px;">
                                    <option value="">Select Type</option>
                                    <option value="0">Flat</option>
                                    <option value="1">Percentage</option>
                                </select>
                            </div>
                            
                            <div id="flat">
                                <input type="text" name="delivery_cost" placeholder="Delivery Cost *">
                            </div>
                            
                            <div id="percentage">
                                <input type="text" name="delivery_percentage" placeholder="Delivery Percent *">
                            </div>
                        </fieldset>
                        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                        <button type="submit" class="payment" style="background-color: #1ABC9C; border: none;">Add Delivery Cost</button>
                    </form>
                </div>
            </div>
        </div>
-->


        </div>
    </div>
@stop