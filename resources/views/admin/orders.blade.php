@extends('layouts.front.master')
@section('body')

    <!-- Banner -->
    <div class="banner-top">
        <div class="container">
            <h3>All Orders</h3>
            <h4><a href="{{ url('/')}}">Home</a><label>/</label>Orders</h4>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- ./Banner -->

    <div class="typrography" style="background-color: #f1f2f3;">
        <div class="container">
            <div class="spec">
                <h3>All Orders</h3>
                <div class="ser-t">
                    <b></b>
                    <span style="background-color: #f1f2f3;"><i></i></span>
                    <b class="line"></b>
                </div>
            </div>
            <div class="col-md-6">
                <input type="search" class="light-table-filter form-control" data-table="order-table"
                       placeholder="Quick Filter">
            </div>
            <div class="clearfix"></div>
            <br>
            <div class="bs-example" data-example-id="simple-table">

                <table class="order-table table" style="font-size: 18px;">

                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Order Ref</th>
                        <th>Delivery Address</th>
                        <th>City</th>
                        <th>State</th>
                        <th>Mobile</th>
                        <th>Product</th>
                        <th>Price</th>
                        <th>Quantity</th>

                        <th>Total</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($orders as $order)

                        <?php

                        $product = \App\Product::where('id', $order->product_id)->first();
                        $orderitem = \App\Order::where('id', $order->order_id)->first();
                        $unit = \App\Unit::where('id', $order->unit_id)->first();
                        ?>
                        <tr>
                            <td>{{$orderitem->fullname}}</td>
                            <td>{{$orderitem->order_ref}}</td>
                            <td>{{$orderitem->delivery_address}}</td>
                            <td>{{$orderitem->city}}</td>
                            <td>{{$orderitem->state}}</td>
                            <td>{{$orderitem->mobile_1}}</td>
                            <td>{{$product->name}}</td>
                            <td>{{$unit->price}}</td>
                            <td>{{$order->quantity}}</td>

                            <td>&#8364;{{(int)$unit->price * (int)$order->quantity}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

        </div>
        <script>
            window.onLoad = function () {
                if ($('#user_id').val().length == 0) {
                    $('#proceed').attr('disabled', true);
                }
            }();

            function logCart(item) {
                console.log(item);
            }
        </script>
    </div>
    <script type="text/javascript">
        (function (document) {
            'use strict';

            var LightTableFilter = (function (Arr) {

                var _input;

                function _onInputEvent(e) {
                    _input = e.target;
                    var tables = document.getElementsByClassName(_input.getAttribute('data-table'));
                    Arr.forEach.call(tables, function (table) {
                        Arr.forEach.call(table.tBodies, function (tbody) {
                            Arr.forEach.call(tbody.rows, _filter);
                        });
                    });
                }

                function _filter(row) {
                    var text = row.textContent.toLowerCase(), val = _input.value.toLowerCase();
                    row.style.display = text.indexOf(val) === -1 ? 'none' : 'table-row';
                }

                return {
                    init: function () {
                        var inputs = document.getElementsByClassName('light-table-filter');
                        Arr.forEach.call(inputs, function (input) {
                            input.oninput = _onInputEvent;
                        });
                    }
                };
            })(Array.prototype);

            document.addEventListener('readystatechange', function () {
                if (document.readyState === 'complete') {
                    LightTableFilter.init();
                }
            });

        })(document);
    </script>
@stop