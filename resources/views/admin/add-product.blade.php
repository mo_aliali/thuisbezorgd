@extends('layouts.front.adminMaster')

@section('body')

    <!-- Banner -->
    <div class="banner-top">
        <div class="container">
            <h3>Add Store</h3>
            <h4><a href="{{ url('/')}}">Home</a><label>/</label>Add Store</h4>
            <div class="clearfix"></div>
        </div>
    </div>

    <!-- Second Content -->
    <div class="product">
        <div class="container">

            <div class="spec">
                <h3>Add Store</h3>
                <div class="ser-t">
                    <b></b>
                    <span><i></i></span>
                    <b class="line"></b>
                </div>
            </div>

            @if (Session::has('success'))
                <div class="container">
                    <div class="alert alert-success"> {{ Session::get('success') }}</div>
                </div>
            @elseif (Session::has('fail'))
                <div class="container">
                    <div class="alert alert-danger"> {{ Session::get('fail') }}</div>
                </div>
            @endif
            <div class="add-product">
                <div class="main-addProduct">


                    <div class="form-style-5">
                        <form action="{{URL::route('storeProduct')}}" method="post" enctype="multipart/form-data">
                            <fieldset>
                                <legend><span class="number">1</span> Item Info</legend>

                                <div {{ ($errors->has('item_name')) ? 'has-error' : ''}} >
                                    <input type="text" name="item_name" placeholder="Item Name *" required>
                                    @if ($errors->has('item_name'))
                                        {{ $errors->first('item_name') }}
                                    @endif
                                </div>

                                <div {{ ($errors->has('item_details ')) ? 'has-error' : ''}} >
                                    <textarea name="item_details" placeholder="Item Details *" required></textarea>
                                    @if ($errors->has('item_details'))
                                        {{ $errors->first('item_details') }}
                                    @endif
                                </div>

                                <div {{ ($errors->has('item_tag')) ? 'has-error' : ''}} >
                                    <input type="text" name="item_tag" placeholder="Item Tag *" required>
                                    @if ($errors->has('item_tag'))
                                        {{ $errors->first('item_tag') }}
                                    @endif
                                </div>

                                <label for="item_category">Item Category:</label>
                                <div {{ ($errors->has('item_catego')) ? 'has-error' : ''}} >
                                    <select id="item_category" name="item_category">
                                        <option value="">Select Category</option>
                                        @foreach($categories as $category)
                                            <option value="{{$category->id}}">{{$category->name}}</option>
                                        @endforeach
                                        <option value="Other">Other, please specify</option>
                                    </select>
                                </div>

                                <label for="item_image">Item Image:</label>
                                <label class="btn btn-default btn-file">
                                    Select File
                                    <input type="file" style="display:none;" name="item_image">
                                </label>
                                <span class='label label-info' id="upload-file-info"></span>

                                <!-- <label class="btn btn-primary" for="my-file-selector">
                <input id="my-file-selector" type="file" style="display:none;" onchange="$('#upload-file-info').html($(this).val());">
                Button Text Here
            </label>
            <span class='label label-info' id="upload-file-info"></span> -->

                            </fieldset>

                            <fieldset>
                                <legend><span class="number">2</span> Item Units</legend>

                                <label for="item_unit1">Item Unit 1:</label>
                                <select name="item_unit1" id="item_unit"
                                        onchange="showfield(this.options[this.selectedIndex].value)">
                                    @foreach($unit_types as $unit_type)
                                        <option value="{{$unit_type->id}}">{{$unit_type->name}}</option>
                                    @endforeach
                                </select>

                                <input type="number" name="item_amount1" placeholder="Item Amount">

                                <label for="item_unit1">Item Unit 2:</label>
                                <select name="item_unit2" id="item_unit"
                                        onchange="showfield(this.options[this.selectedIndex].value)">
                                    @foreach($unit_types as $unit_type)
                                        <option value="{{$unit_type->id}}">{{$unit_type->name}}</option>
                                    @endforeach
                                </select>

                                <input type="number" name="item_amount2" placeholder="Item Amount">

                                <label for="item_unit3">Item Unit 3:</label>
                                <select name="item_unit3" id="item_unit"
                                        onchange="showfield(this.options[this.selectedIndex].value)">
                                    @foreach($unit_types as $unit_type)
                                        <option value="{{$unit_type->id}}">{{$unit_type->name}}</option>
                                    @endforeach
                                </select>

                                <input type="number" name="item_amount3" placeholder="Item Amount">

                                <label for="item_unit4">Item Unit 4:</label>
                                <select name="item_unit4" id="item_unit"
                                        onchange="showfield(this.options[this.selectedIndex].value)">
                                    @foreach($unit_types as $unit_type)
                                        <option value="{{$unit_type->id}}">{{$unit_type->name}}</option>
                                    @endforeach
                                </select>

                                <input type="number" name="item_amount4" placeholder="Item Amount">

                                <label for="item_unit5">Item Unit 5:</label>
                                <select name="item_unit5" id="item_unit"
                                        onchange="showfield(this.options[this.selectedIndex].value)">
                                    @foreach($unit_types as $unit_type)
                                        <option value="{{$unit_type->id}}">{{$unit_type->name}}</option>
                                    @endforeach
                                </select>

                                <input type="number" name="item_amount5" placeholder="Item Amount">


                            </fieldset>
                            <input type="submit" value="Add Product"/>
                            <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}"/>

                        </form>
                    </div>


                <!--<form action="{{URL::route('storeProduct')}}" method="post" enctype="multipart/form-data">
                        <div class="own-key {{ ($errors->has('item_name')) ? 'has-error' : ''}}">
              						<input type="text" value="{{old('item_name') ? old('item_name') : 'Item name'}}" placeholder="Item name"  name="item_name" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Item Name';}" required="">
              						<div class="clearfix"></div>
                          @if ($errors->has('item_name'))
                    {{ $errors->first('item_name') }}
                @endif
                        </div>

              <div class="own-key {{ ($errors->has('item_details ')) ? 'has-error' : ''}}">
              						<input type="text" value="{{old('item_details') ? old('item_details') : 'Item details'}}" placeholder="Item details"  name="item_details" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Item details';}" required="">
              						<div class="clearfix"></div>
                          @if ($errors->has('item_details'))
                    {{ $errors->first('item_details') }}
                @endif
                        </div>

              <div class="own-key {{ ($errors->has('item_tag')) ? 'has-error' : ''}}">
              						<input type="text" value="{{old('item_tag') ? old('item_tag') : 'Item tag'}}" placeholder="Item tag"  name="item_tag" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Item Tag';}" required="">
              						<div class="clearfix"></div>
                          @if ($errors->has('item_tag'))
                    {{ $errors->first('item_tag') }}
                @endif
                        </div>

              <div class="own-key-select {{ ($errors->has('item_catego')) ? 'has-error' : ''}}" style="display:block; width: 60% !important;">
                          <select name="item_category" onchange="other()" id="item_category" onchange="showfield(this.options[this.selectedIndex].value)">
                            <option value="">Select Category</option>
                            @foreach($categories as $category)
                    <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        <option value="Other">Other, please specify</option>
                      </select>

                      <div id="div1" class="clearfix"></div>
                    </div>

                    <div class="own-key-select">
                      <select name="item_unit1" id="item_unit" onchange="showfield(this.options[this.selectedIndex].value)">
                            @foreach($unit_types as $unit_type)
                    <option value="{{$unit_type->id}}">{{$unit_type->name}}</option>
                            @endforeach
                        </select>
                      </div>

                      <div class="own-key-item">
                                    <input type="number" value="{{old('item_amount1') ? old('item_amount1') : 'Item Amount'}}" placeholder="Item Amount"  name="item_amount1" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Item Amount';}">
              						<div class="clearfix"></div>
              					</div>
                        <h4 class="amount">Amount</h4>

                        <div class="own-key-select">
                          <select name="item_unit2" id="item_unit" onchange="showfield(this.options[this.selectedIndex].value)">
                            @foreach($unit_types as $unit_type)
                    <option value="{{$unit_type->id}}">{{$unit_type->name}}</option>
                            @endforeach
                        </select>
                      </div>

                      <div class="own-key-item">
                                    <input type="number" value="{{old('item_amount2') ? old('item_amount2') : 'Item Amount'}}" placeholder="Item Amount"  name="item_amount2" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Item Amount';}">
              						<div class="clearfix"></div>
              					</div>
                        <h4 class="amount">Amount</h4>

                        <div class="own-key-select">
                          <select name="item_unit3" id="item_unit" onchange="showfield(this.options[this.selectedIndex].value)">
                            @foreach($unit_types as $unit_type)
                    <option value="{{$unit_type->id}}">{{$unit_type->name}}</option>
                            @endforeach
                        </select>
                      </div>

                      <div class="own-key-item">
                                    <input type="number" value="{{old('item_amount3') ? old('item_amount3') : 'Item Amount'}}" placeholder="Item Amount"  name="item_amount3" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Item Amount';}">
              						<div class="clearfix"></div>
              					</div>
                        <h4 class="amount">Amount</h4>

                        <div class="own-key-select">
                          <select name="item_unit4" id="item_unit" onchange="showfield(this.options[this.selectedIndex].value)">
                            @foreach($unit_types as $unit_type)
                    <option value="{{$unit_type->id}}">{{$unit_type->name}}</option>
                            @endforeach
                        </select>
                      </div>

                      <div class="own-key-item">
                                    <input type="number" value="{{old('item_amount4') ? old('item_amount4') : 'Item Amount'}}" placeholder="Item Amount"  name="item_amount4" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Item Amount';}">
              						<div class="clearfix"></div>
              					</div>
                        <h4 class="amount">Amount</h4>

                        <div class="own-key-select">
                          <select name="item_unit5" id="item_unit" onchange="showfield(this.options[this.selectedIndex].value)">
                            @foreach($unit_types as $unit_type)
                    <option value="{{$unit_type->id}}">{{$unit_type->name}}</option>
                            @endforeach
                        </select>
                      </div>

                      <div class="own-key-item">
                                    <input type="number" value="{{old('item_amount5') ? old('item_amount5') : 'Item Amount'}}" placeholder="Item Amount"  name="item_amount5" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Item Amount';}">
              						<div class="clearfix"></div>
              					</div>
                        <h4 class="amount">Amount</h4>
                        <br>
                          <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                        <input type="submit" value="Submit" class="productBtn">

                      </form>-->
                </div>
            </div>


        </div>
    </div>

    <script type="text/javascript">
        function showfield(name) {
            if (name == 'Other') document.getElementById('div1').innerHTML = 'Other: <input type="text" name="other" />';
            else document.getElementById('div1').innerHTML = '';
        }
    </script>

@stop
