@extends('layouts.front.adminMaster')

@section('body')

    <!-- Banner -->
    <div class="banner-top">
        <div class="container">
            <h3>Profile</h3>
            <h4><a href="{{ url('/')}}">Home</a><label>/</label>Profile</h4>
            <div class="clearfix"></div>
        </div>
    </div>

    <!-- Second Content -->
    <div class="product">
        <div class="container">

            <div class="spec">
                <h3>Profile</h3>
                <div class="ser-t">
                    <b></b>
                    <span><i></i></span>
                    <b class="line"></b>
                </div>
            </div>

            <div class="add-product">
                <div class="main-addProduct">

                    <div class="form-style-5">
                        <fieldset>
                            <legend>Contact Information <span style="float: right; font-size:12px;"><a
                                            href="{{ url('admin-edit-profile') }}">Edit</a></span></legend>
                            <div class="contact-info">
                                <h5>Mohammed Ali</h5>
                                <p>m.ali8nl@gmail.com</p>
                                <p>+31 614959413</p>
                                <p>Amsterdam, Netherlands</p>
                            </div>
                        </fieldset>
                        <br><br>

                        <fieldset>
                            <legend>Billing Address <span style="float: right; font-size:12px;"<a
                                        href="{{ url('admin-edit-profile') }}">Edit</a></span></legend>
                            <div class="billing-info">
                                <h5>Mohammed Ali</h5>
                                <p>+31 614959413</p>
                                <p>Amsterdam, Netherlands</p>
                            </div>
                        </fieldset>
                        <br><br>

                        <fieldset>
                            <legend>Delivery Address <span style="float: right; font-size:12px;"><a
                                            href="{{ url('admin-edit-profile') }}">Edit</a></span></legend>
                            <div class="billing-info">
                                <h5>Mohammed Ali - Ali</h5>
                                <p>+31 614959413</p>
                                <p>Amsterdam, Netherlands</p>
                            </div>
                        </fieldset>
                    </div>

                </div>
            </div>

        </div>
    </div>

@stop
