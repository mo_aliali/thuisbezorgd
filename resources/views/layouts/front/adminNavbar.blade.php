<div class="header">

        <div class="container">

            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="logo-wrapper_admin">
                            <a href="{{ url('/')}}">
                                <img src="images/logo/logo.png" alt=""/>
                                <div class="logo_text_admin">
                                    <h1>Bezorgd</h1>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="head-t">
                <ul class="card" style="box-shadow: none;">
                    <li><a href="{{ url('/') }}" ><i class="fa fa-ship" aria-hidden="true"></i>Home</a></li>
                    {{--<li><a href="{{ url('admin-dashboard') }}" ><i class="fa fa-ship" aria-hidden="true"></i>Admin</a></li>--}}
                </ul>
            </div>

                <div class="nav-top">
                    <nav class="navbar navbar-default">

                    <div class="navbar-header nav_2">
                        <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>


                    </div>
                    <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                        <ul class="nav navbar-nav ">
                            <li class="active"><a href="{{ url('add-product')}}" class="hyper"><span>Add Store</span></a></li>

                            <li><a href="{{ url('view-profile')}}" class="hyper"><span>View Profile</span></a></li>

                            <li><a href="{{ url('add-admin')}}" class="hyper"><span>Add Admin</span></a></li>
                            
                            <li><a href="{{ url('add-delivery')}}" class="hyper"><span>Add Delivery Charge</span></a></li>

                            <li><a href="{{ route('getOrders')}}" class="hyper"><span>All Orders</span></a></li>
                        </ul>
                    </div>
                    </nav>
                     <div class="cart" >

                        <span class="fa fa-shopping-cart my-cart-icon"><span class="badge badge-notify my-cart-badge"></span></span>
                    </div>
                    <div class="clearfix"></div>
                </div>

                </div>
</div>