<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <div class="logo-wrapper">
                <a href="{{ url('/')}}">
                    <img src="images/logo/logo.png" alt=""/>
                    <div class="logo_text">
                        <h1>Bezorgd</h1>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="header">

    <div class="container">
        <div class="head-t">
            <ul class="card" style="box-shadow: none;">
                @if(!Auth::check())
                    <li><a href="{{ url('login')}}"><i class="fa fa-user" aria-hidden="true"></i>Login</a></li>
                    <li><a href="{{ url('register')}}"><i class="fa fa-arrow-right" aria-hidden="true"></i>Register</a>
                    </li>
                @endif
                @if(Auth::check())
                    @if(Auth::user()->role_id == 1)
                        <li><a href="{{ url('admin') }}"><i class="fa fa-ship" aria-hidden="true"></i>Admin</a></li>
                    @endif
                @endif
                @if(Auth::check())
                    <li><a href="{{ url('logout') }}"><i class="fa fa-ship" aria-hidden="true"></i>Logout</a></li>
                @endif
            </ul>
            <div class="nav-top">
                <nav class="navbar navbar-default">

                    <div class="navbar-header nav_2">
                        <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse"
                                data-target="#bs-megadropdown-tabs">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>


                    </div>
                    <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                        <ul class="nav navbar-nav ">
                            <li class=" active"><a href="{{ url('/')}}" class="hyper "><span>Home</span></a></li>

                            <li class="dropdown ">
                                <a href="#" class="dropdown-toggle  hyper" data-toggle="dropdown"><span>Food Items<b
                                                class="caret"></b></span></a>
                                <ul class="dropdown-menu multi">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <ul class="multi-column-dropdown">

                                                <li><a href="{{ url('food-items#Hollandse-Nieuwe')}}"><i
                                                                class="fa fa-angle-right" aria-hidden="true"></i>Hollandse
                                                        Nieuwe</a></li>
                                                <li><a href="{{ url('food-items#Stroopwafel')}}"><i
                                                                class="fa fa-angle-right" aria-hidden="true"></i>Stroopwafel</a>
                                                </li>
                                                <li><a href="{{ url('food-items#Kroket')}}"><i class="fa fa-angle-right"
                                                                                               aria-hidden="true"></i>Kroket</a>
                                                </li>
                                                <li><a href="{{ url('food-items#Patat')}}"><i class="fa fa-angle-right"
                                                                                              aria-hidden="true"></i>Patat</a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Poffertjes</a>
                                                </li>
                                                <li><a href="#"> <i class="fa fa-angle-right" aria-hidden="true"></i>Bitterballen</a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Drop</a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Kaas
                                                    </a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Hagelslag</a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Oliebollen</a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Erwtensoep
                                                        / snert</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Stamppot</a>
                                                </li>
                                                <li><a href="{{ url('food-items#Rookworst')}}"><i
                                                                class="fa fa-angle-right" aria-hidden="true"></i>Rookworst</a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Lekkerbekje
                                                        / Kibbeling</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Pannenkoeken</a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Rice</a>
                                                </li>

                                            </ul>

                                        </div>

                                        <div class="col-sm-3">
                                            <ul class="multi-column-dropdown">

                                                <li><a href="{{ url('food-items#Hollandse-Nieuwe')}}"><i
                                                                class="fa fa-angle-right" aria-hidden="true"></i>Hollandse
                                                        Nieuwe</a></li>
                                                <li><a href="{{ url('food-items#Stroopwafel')}}"><i
                                                                class="fa fa-angle-right" aria-hidden="true"></i>Stroopwafel</a>
                                                </li>
                                                <li><a href="{{ url('food-items#Kroket')}}"><i class="fa fa-angle-right"
                                                                                               aria-hidden="true"></i>Kroket</a>
                                                </li>
                                                <li><a href="{{ url('food-items#Patat')}}"><i class="fa fa-angle-right"
                                                                                              aria-hidden="true"></i>Patat</a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Poffertjes</a>
                                                </li>
                                                <li><a href="#"> <i class="fa fa-angle-right" aria-hidden="true"></i>Bitterballen</a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Drop</a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Kaas
                                                    </a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Hagelslag</a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Oliebollen</a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Erwtensoep
                                                        / snert</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Stamppot</a>
                                                </li>
                                                <li><a href="{{ url('food-items#Rookworst')}}"><i
                                                                class="fa fa-angle-right" aria-hidden="true"></i>Rookworst</a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Lekkerbekje
                                                        / Kibbeling</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Pannenkoeken</a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Rice</a>
                                                </li>

                                            </ul>

                                        </div>


                                        <div class="col-sm-3">
                                            <ul class="multi-column-dropdown">

                                                <li><a href="{{ url('food-items#Hollandse-Nieuwe')}}"><i
                                                                class="fa fa-angle-right" aria-hidden="true"></i>Hollandse
                                                        Nieuwe</a></li>
                                                <li><a href="{{ url('food-items#Stroopwafel')}}"><i
                                                                class="fa fa-angle-right" aria-hidden="true"></i>Stroopwafel</a>
                                                </li>
                                                <li><a href="{{ url('food-items#Kroket')}}"><i class="fa fa-angle-right"
                                                                                               aria-hidden="true"></i>Kroket</a>
                                                </li>
                                                <li><a href="{{ url('food-items#Patat')}}"><i class="fa fa-angle-right"
                                                                                              aria-hidden="true"></i>Patat</a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Poffertjes</a>
                                                </li>
                                                <li><a href="#"> <i class="fa fa-angle-right" aria-hidden="true"></i>Bitterballen</a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Drop</a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Kaas
                                                    </a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Hagelslag</a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Oliebollen</a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Erwtensoep
                                                        / snert</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Stamppot</a>
                                                </li>
                                                <li><a href="{{ url('food-items#Rookworst')}}"><i
                                                                class="fa fa-angle-right" aria-hidden="true"></i>Rookworst</a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Lekkerbekje
                                                        / Kibbeling</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Pannenkoeken</a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Rice</a>
                                                </li>

                                            </ul>

                                        </div>


                                        <div class="col-sm-3 w3l">
                                            <a href="#"><img src="{{URL::asset('images/me.png')}}"
                                                             class="img-responsive" alt=""></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </ul>
                            </li>


                            <li class="dropdown">

                                <a href="#" class="dropdown-toggle hyper" data-toggle="dropdown"><span> Provision Items <b
                                                class="caret"></b></span></a>
                                <ul class="dropdown-menu multi multi1">
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <ul class="multi-column-dropdown">
                                                <li><a href="{{ url('provision-items#cornflakes')}}"><i
                                                                class="fa fa-angle-right" aria-hidden="true"></i>
                                                        Cornflakes </a></li>
                                                <li><a href="{{ url('provision-items#cornflakes_kellogs')}}"><i
                                                                class="fa fa-angle-right" aria-hidden="true"></i>
                                                        Cornflakes (kellogs) </a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>
                                                        Sugar (cubes) </a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>
                                                        Sugar (granulated) </a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>
                                                        Lipton </a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Toptea</a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Sardine</a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Icing
                                                        sugar</a></li>
                                                <li><a href="care.html"><i class="fa fa-angle-right"
                                                                           aria-hidden="true"></i>Eggs</a></li>

                                            </ul>

                                        </div>
                                        <div class="col-sm-3">

                                            <ul class="multi-column-dropdown">
                                                <li><a href="#"> <i class="fa fa-angle-right" aria-hidden="true"></i>Guesha</a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Cornedbeef</a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Flour</a>
                                                </li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Butter
                                                        (sima)</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Butter
                                                        (blueband)</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Baking
                                                        powder (Foster Clark)</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Milk
                                                        (cowbell)</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Milk
                                                        (condensed)</a></li>

                                            </ul>

                                        </div>
                                        <div class="col-sm-3">

                                            <ul class="multi-column-dropdown">
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>
                                                        Cake flavour (strawberry) </a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>
                                                        Cake flavour (vanilla)</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Water
                                                        (bottle)</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Water
                                                        (bag)</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Water
                                                        (half bag)</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Milk
                                                        (peak)</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Milk
                                                        (dano)</a></li>
                                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i>Milk
                                                        (loya)</a></li>


                                            </ul>
                                        </div>
                                        <div class="col-sm-3 w3l">
                                            <a href="care.html"><img src="{{URL::asset('images/me1.png')}}"
                                                                     class="img-responsive" alt=""></a>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </ul>
                            </li>

                            <li><a href="{{ url('contact')}}" class="hyper"><span>Contact Us</span></a></li>
                        </ul>
                    </div>
                </nav>

                @if(Auth::check())
                    <div class="cart">
                        <div data-toggle="modal" data-target="#modal" class="title m-b-md">
                            <span class="fa fa-shopping-cart my-cart-icon"><span class="badge"
                                                                                 id="c_count">{{ Cart::count() }}</span></span>
                        </div>
                    </div>
                @endif

                <div class="clearfix"></div>
            </div>

        </div>
    </div>
</div>