<!--footer-->
<div class="footer">
	<div class="container">
		<div class="col-md-3 footer-grid">
			<h3>About Us</h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
		</div>
		<div class="col-md-3 footer-grid ">
			<h3>Menu</h3>
			<ul>
				<li><a href="{{ url('/')}}">Home</a></li>
				<li><a href="{{ url('food-items')}}">Food Items</a></li>
				<li><a href="{{ url('provision-items')}}">Provision Items</a></li>
				<li><a href="{{ url('contact')}}">Contact Us</a></li>
			</ul>
		</div>
		<div class="col-md-3 footer-grid ">
			<h3>Customer Services</h3>
			<ul>
				<li><a href="{{ url('delivery')}}">Delivery</a></li>
				<li><a href="{{ url('terms')}}">Terms &amp; Conditions</a></li>
				<li><a href="{{ url('faq')}}">Faqs</a></li>
				<li><a href="{{ url('contact')}}">Contact Us</a></li>
			</ul>
		</div>
		<div class="col-md-3 footer-grid">
			<h3>My Account</h3>
			<ul>
				<li><a href="{{ url('login')}}">Login</a></li>
				<li><a href="{{ url('register')}}">Register</a></li>
				<li><a href="{{ url('#')}}">Forgot Password</a></li>

			</ul>
		</div>
		<div class="clearfix"></div>
				<div class="footer-bottom">
				<p class="fo-para">
					<!-- Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris -->
				</p>
				<ul class="social-fo">
					<!-- <li><a href="#" class=" face"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href="#" class=" twi"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
					<li><a href="#" class=" pin"><i class="fa fa-pinterest-p" aria-hidden="true"></i></a></li>
					<li><a href="#" class=" dri"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li> -->
				</ul>
				<div class=" address">
					<div class="col-md-4 fo-grid1">
							<p><i class="fa fa-home" aria-hidden="true"></i>6 Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
					</div>
					<div class="col-md-4 fo-grid1">
							<p><i class="fa fa-phone" aria-hidden="true"></i>+020 123 45 67 </p>
					</div>
					<div class="col-md-4 fo-grid1">
						<p><a href="mailto:marketboyng@gmail.com"><i class="fa fa-envelope-o" aria-hidden="true"></i>supermarket@mogroupp.com</a></p>
					</div>
					<div class="clearfix"></div>

					</div>
			</div>
		<div class="copy-right">
			<p> &copy; 2018 MO. All Rights Reserved </p>
		</div>
	</div>
</div>
<!-- //footer-->

<!-- Modal HTML Markup -->
<div id="modal" class="modal fade">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel"><span class="glyphicon glyphicon-shopping-cart"></span> My Cart</h4>
						</div><!-- /.modal-header -->
						<div class="modal-body">
							<table class="table table-hover table-responsive">
								<thead>
									<tr>
										<th></th>
										<th>Item</th>
										<th>Amount</th>
										<th>Quantity</th>
										<th>Total</th>
										<th></th>
									</tr>
								</thead>
								<tbody id="cart">
              @if (sizeof(Cart::content())>0)
               @foreach (Cart::content() as $item)
			   						<?php $row = 't-row'. $item->rowid ?>
									<tr id="{{$row}}">
										<td class="text-center" style="width: 30px;">
											<img src="{{$item->options->image}}" width="30px" height="30px">
										</td>
										<td>{{$item->name}}</td>
										<td title="Unit Price">&#8364; {{$item->price}}</td>
										<?php $cart= "cart" . $item->rowid; ?>
										<?php $price= "price" . $item->rowid; ?>
										<td title="Quantity"><input id="{{$cart}}" onfocus="imminentChange('{{$item->rowid}}')" onblur="finishedChange('{{$item->rowid}}','{{$item->price}}')" type="number" min="1" style="width: 40px;" value="{{$item->qty}}" class=""/></td>
										<td title="Total" id="{{$price}}">&#8364; {{$item->subtotal}}</td>
										<td title="Remove from Cart" class="text-center" style="width: 30px;"><a href="javascript:void(0);" onclick="removeCart('{{$item->rowid}}')" class="btn btn-xs btn-danger ' + classProductRemove + '">X</a></td>
    							</tr>
									@endforeach
								@endif
								</tbody>
							</table>
						</div><!-- /.modal-body -->
						<div class="modal-footer">
							<button type="button" class="btn btn-default"><a href="{{ url('checkout') }}">Checkout</a></button>
						</div><!-- /.modal-footer -->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<!-- tabs -->
<script src="{{URL::asset('js/easyResponsiveTabs.js')}}" type="text/javascript"></script>
	<script type="text/javascript">
		$(document).ready(function () {
			$('#horizontalTab').easyResponsiveTabs({
			type: 'default', //Types: default, vertical, accordion
			width: 'auto', //auto or any width like 600px
			fit: true   // 100% fit in a container
			});
		});
	</script>
<!-- //tabs -->


<!-- smooth scrolling -->
	<script type="text/javascript">
		$(document).ready(function() {
		/*
			var defaults = {
			containerID: 'toTop', // fading element id
			containerHoverID: 'toTopHover', // fading element hover id
			scrollSpeed: 1200,
			easingType: 'linear'
			};
		*/
		$().UItoTop({ easingType: 'easeOutQuart' });
		});
	</script>
	<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- //smooth scrolling -->
<!-- for bootstrap working -->
<!-- //for bootstrap working -->
<script type='text/javascript' src="{{URL::asset('js/jquery.mycart.js')}}"></script>
	<script>
	var quantity = 0;
	function addToCart(id){
		var isLoggedIn = {{Auth::guest()}} + "0";
		if(isLoggedIn == 10){
			setTimeout(function() {
        swal({
            title: "Alert!",
            text: "Please Kindly login to add to cart",
            type: "error"
        }, function() {
            window.location = "{{route('getLogin')}}";
        });
    }, 1000);
		};
		$('#idCart' + id).addClass('green');
		$('#idCart2' + id).addClass('green');
		var baseUrl = "{{ url('/') }}";
		var unit_id = document.getElementById('unit' + id).value;
		console.log(unit_id);
	      $.get(baseUrl + '/cart/add/' + id + '/' + unit_id, function (data) {
			  console.log(data.cart);
				var items = data.cart;
				document.getElementById('c_count').innerText = data.count;
				// for (var i=0; i<items.length; $i++ ){
				// 		$items[i] =
				// }
				// document.getElementById('cart').innerHtml = "";
				$('#cart').empty();
				$.each(items, function($val, $item){
					$('#cart').append("<tr id=\"t-row" + $item.rowid + "\"><td class=\"text-center\" style=\"width: 30px;\"> <img src=\"" + $item.options.image + "\"width=\"30px\" height=\"30px\"></td><td>" + $item.name + "</td><td title=\"Unit Price\">&#8364;" + $item.price + "</td><td title=\"Quantity\"><input onblur=\"finishedChange(" + '\'' + $item.rowid + '\'' + ")\" onfocus=\"imminentChange(" + '\'' + $item.rowid + '\'' + ")\" id=\"cart" + $item.rowid + "\" type=\"number\" min=\"1\" style=\"width: 40px;\" value=\"" + $item.qty + "\"/></td><td title=\"Total\" id=\"price" + $item.rowid + "\" \>&#8364;" + $item.subtotal + "</td><td title=\"Remove from Cart\" class=\"text-center\" style=\"width: 30px;\"><a style=\"display: block;\" href=\"javascript:void(0);\" onclick=\"removeCart(" + '\'' + $item.rowid + '\'' + ")\" class=\"btn btn-xs btn-danger\">X</a></td></tr>")
				})
		//   $('#price' + data.product_id).text(data.price);
      });
	}
	function imminentChange($item){
		console.log($item);
		quantity = document.getElementById('cart' + $item).value;
		console.log(quantity);
	}
function finishedChange($item, price){
		console.log($item, price);
		var baseUrl = "{{ url('/') }}";
		// console.log($row);
		var n_quantity = document.getElementById('cart' + $item).value;
		document.getElementById('price' + $item).innerText ="$ " + price * n_quantity;
		if(n_quantity == quantity){
			console.log('no changes made');
		}
		else{
			console.log('it has been changed so fire ajax');
				      $.get(baseUrl + '/cart/update/' + $item + '/' + n_quantity, function (data) {
			  console.log(data);
			  document.getElementById('c_count').innerText = data.count;
			  var n_quantity = document.getElementById('cart' + $item).value;
      });
		}

	}
function removeCart(item){
		var baseUrl = "{{ url('/') }}";
			$.get(baseUrl + '/cart/remove/' + item , function (data) {
			  console.log(data);
			$('#t-row' + item).empty();
			document.getElementById('c_count').innerText = data.count;
      });
		}
function dType(val){
    if (val == 0){
        $('#percentage').addClass('hidden');
        console.log(val);
    }
    else{
        $('#flat').addClass('hidden');
        console.log(val);

    }
}
	</script>
