@extends('layouts.front.master')
@section('body')

<!-- Banner -->
<div class="banner-top">
    <div class="container">
        <h3>Checkout</h3>
        <h4><a href="{{ url('/')}}">Home</a><label>/</label>Checkout</h4>
        <div class="clearfix"></div>
    </div>
</div>
<!-- ./Banner -->

<div class="typrography" style="background-color: #f1f2f3;">
    <div class="container">
        <div class="spec">
            <h3>Checkout</h3>
            <div class="ser-t">
                <b></b>
                <span style="background-color: #f1f2f3;"><i></i></span>
                <b class="line"></b>
            </div>
        </div>

        <div class="checkout-container">
            <form method="POST" action="{{ route('pay') }}">
                <ul class="row">
                    <li class="col-md-6 col-xs-12 payment-info">
                        <h3 style="color: #039445">Payment</h3>
                        <div class="box pay-option">
                            <h4>Billing Information</h4>
                            <hr class="dotted">
                            <h5>Payment Method</h5>
                            <div class="pay-methods">
                                @if (App\User::find(1)->perishable())
                                <p class="alert alert-danger">you have a perishable item in your cart and so you can only pay using the card method</p>
                                <select name="payment_method" class="payment-methods" style="color: #495d76">
                                    <option title='Pay with card' value='paycard' style="color: #495d76">Pay with card</option>
                                </select>
                                @else
                                <select name="payment_method" class="payment-methods" style="color: #495d76">
                                    <option title='Pay with card' value='paycard' style="color: #495d76">Pay with card</option>
                                  <!--  <option title='Pay Cash on delivery' value='paycash' style="color: #495d76">Pay Cash on delivery</option>
                                    <option title='Pay with POS on delivery' value='paypos' style="color: #495d76">Pay with POS</option> -->
                                </select>
                                @endif
                            </div>
                            <h4>Delivery Address</h4>
                            <hr class="dotted">
                            @if(Auth::user())
                            <p style="margin-top: 10px; margin-bottom: 10px;"><small>Please provide address where items will be delivered You can either edit it or use the previous address.</small></p>
                            <div class="form-group">
                                <label class="form-inputs" for="address">Home Address:</label>
                                <br><br>
                                <input type="text" value="{{Auth::user()->billing? Auth::user()->billing->delivery_address: ""}}" name="delivery_address" class="form-control input-fields add" required>
                            </div>
                            <span style="display: inline-block; padding-right: 85px;">
                                <label class="form-inputs" for="city">City:</label>
                                <br><br>
                                <input value="{{Auth::user()->billing? Auth::user()->billing->city: ""}}" type="text" name="city" class="form-control input-fields" required>
                            </span>
                            <span style="display: inline-block;">
                                <label class="form-inputs" for="state" style="display: block">City:</label>
                                <br><br>
                                <select name="state" class="select-states" style="color: #495d76" required>
                                    @if(Auth::user()->billing && Auth::user()->billing->city == "North-Holland")
                                    <option title='North-Holland' value="North-Holland" style="color: #495d76" selected="selected">North-Holland</option>
                                    @else
                                    <option title='North-Holland' value="North-Holland" style="color: #495d76">North-Holland</option>
                                    @endif
                                    @if(Auth::user()->billing && Auth::user()->billing->city == "South-Holland")
                                    <option title='South-Holland' value="South-Holland" style="color: #495d76" selected="selected">South-Holland</option>
                                    @else
                                    <option title='South-Holland' value="South-Holland" style="color: #495d76">South-Holland</option>
                                    @endif
                                    @if(Auth::user()->billing && Auth::user()->billing->city == "The Hague")
                                    <option title='Gelderland' value="Gelderland" style="color: #495d76" selected="selected">Gelderland</option>
                                    @else
                                    <option title='Gelderland' value="Gelderland" style="color: #495d76">Gelderland</option>
                                    @endif
                                </select>
                            </span>
                            <span style="display: inline-block; padding-right: 85px; margin-top: 10px;">
                                <label class="form-inputs" for="city">Mobile 1:</label>
                                <br><br>
                                <input value="{{Auth::user()->billing? Auth::user()->billing->mobile_1: ""}}" name="mobile_1" type="number" class="form-control input-fields" required>
                            </span>
                            
                        </div>
                        @else
                        <p>kindly <a href="{{url('ordered')}}">register</a> or <a href="{{url('ordered')}}">log in</a> to access your billing address. You can't pay without filling the form.</p>
                    </div>
                @endif
                <?php 
                $d_cost = App\User::find(1)->delivery(Cart::total());
                ?>
                <input type="hidden" name="sub_total" value="{{Cart::total()}}">
                <input type="hidden" name="items" value="{{sizeof(Cart::content())}}">
                <input type="hidden" name="delivery" value="{{$d_cost}}">
                <input type="hidden" name="total" value="{{Cart::total() + $d_cost}}">
                <input type="hidden" name="amount" value="{{(Cart::total() + $d_cost) * 100}}">
                <input type="hidden" name="email" value="{{Auth::user()?Auth::user()->email : 'mo@mogroupp.com'}}"> {{-- required --}}
                <input type="hidden" id="user_id" name="user_id" value="{{Auth::user()?Auth::user()->id: ""}}" required>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                {{-- @if(Auth::user()) --}}
                <input id="proceed" onclick="check(this)" type="submit" class="payment pull-right" value="proceed">
                </li>
            <li class="col-md-6 col-xs-12 item-summary">
                <h3 style="color: #039445">Order Summary<small class="pull-right" style="margin-top: 5px;">Edit</small></h3>
                <hr class="line" style="border-bottom: 2px solid #CBCBCB; margin-top: 40px;">
                <div class="row" style="padding-left: 20px;">
                    @if (sizeof(Cart::content())>0)
                    @foreach (Cart::content() as $item)
                    <div class="col">
                        <?php $unit = App\Unit::find($item->options->unit);
                        $product = App\Product::find($item->id);
                        ?>
                        <h4 style="color: #7D8B9D; font-weight: bold;">{{$item->name}}<small class="pull-right" style="padding-right: 20px;">{{$item->qty . " " .$unit->type->name}}</small></h4>
                        <h5 style="color: #7D8B9D; padding-top: 10px; font-size: 16px;">{{$product->description}}<span class="pull-right" style="padding-right: 20px; font-weight: bold;">&#8364; {{$item->subtotal}}</span></h5>
                        <hr class="line" style="border-bottom: 2px solid #CBCBCB; margin-right: 10px;">
                    </div>
                    @endforeach
                    @endif
                </div>
                <div class="row" style="padding-left: 20px;">
                    <div class="col">
                        <h5 style="color: #7D8B9D;font-weight: bold">SubTotal<span class="pull-right" style="padding-right: 20px;">&#8364; {{Cart::total()}}</span></h5>
                        <h5 style="color: #7D8B9D; padding-top: 20px; font-weight: bold;">Delivery<span class="pull-right" style="padding-right: 20px;">&#8364; {{$d_cost}}</span></h5>
                        <h5 style="color: #7D8B9D; padding-top: 20px; font-weight: bold;">Total<span class="pull-right" style="padding-right: 20px; font-size: 18px; color: #9AA2D5;">&#8364; {{Cart::total() + $d_cost}}</span></h5>
                    </div>
                </div>
            </li>
            </ul>
        </form>
</div>

</div>
<script>
    window.onLoad = function(){
        if($('#user_id').val().length == 0){
            $('#proceed').attr('disabled',true);
        }
    }();
    function logCart(item){
        console.log(item);
    }
</script>
</div>
@stop
