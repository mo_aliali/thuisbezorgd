@extends('layouts.front.master')

@section('body')

    <!-- Banner -->
    <div class="banner-top">
        <div class="container">
            <h3>Stores</h3>
            <h4><a href="{{ url('/')}}">Home</a><label>/</label>Featured Items</h4>
            <div class="clearfix"></div>
        </div>
    </div>

    <!-- Second Content -->
    <div class="kic-top">
        <div class="container">

            <div class="spec">
                <h3 class="green">Stores</h3>
                <div class="ser-t">
                    <b></b>
                    <span><i></i></span>
                    <b class="line"></b>
                </div>
            </div>

            <div class="col-md-4 kic-top1">
                <a href="#">
                    <img src="images/ki2.jpg" class="img-responsive" alt="">
                </a>
                <h6>Native Spices</h6>
            </div>
            <div class="col-md-4 kic-top1">
                <a href="#">
                    <img src="images/ki.jpg" class="img-responsive" alt="">
                </a>
                <h6>Beans</h6>
            </div>
            <div class="col-md-4 kic-top1">
                <a href="#">
                    <img src="images/ki1.jpg" class="img-responsive" alt="">
                </a>
                <h6>Noodles</h6>
            </div>
            <div class="col-md-4 kic-top1">
                <a href="#">
                    <img src="images/ki2.jpg" class="img-responsive" alt="">
                </a>
                <h6>Native Spices</h6>
            </div>
            <div class="col-md-4 kic-top1">
                <a href="#">
                    <img src="images/ki.jpg" class="img-responsive" alt="">
                </a>
                <h6>Beans</h6>
            </div>
            <div class="col-md-4 kic-top1">
                <a href="#">
                    <img src="images/ki1.jpg" class="img-responsive" alt="">
                </a>
                <h6>Noodles</h6>
            </div>
            <div class="col-md-4 kic-top1">
                <a href="#">
                    <img src="images/ki2.jpg" class="img-responsive" alt="">
                </a>
                <h6>Native Spices</h6>
            </div>
            <div class="col-md-4 kic-top1">
                <a href="#">
                    <img src="images/ki.jpg" class="img-responsive" alt="">
                </a>
                <h6>Beans</h6>
            </div>
            <div class="col-md-4 kic-top1">
                <a href="#">
                    <img src="images/ki1.jpg" class="img-responsive" alt="">
                </a>
                <h6>Noodles</h6>
            </div>

        </div>
    </div>
@stop
