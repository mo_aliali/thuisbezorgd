@extends('layouts.front.master')
@section('body')
    @if (Session::has('success'))
        <div class="">
            <div class="alert alert-success text-center">
                {{Session::get('success')}}
            </div>
        </div>
    @endif
    <!-- Carousel -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <a href="#"><img class="first-slide" src="images/ba.jpg" alt="First slide"></a>
            </div>

            <div class="item">
                <a href="#"><img class="second-slide " src="images/ba.jpg" alt="Second slide"></a>
            </div>

            <div class="item">
                <a href="#"> <img class="third-slide " src="images/ba.jpg" alt="Third slide"></a>
            </div>
        </div>
    </div>
    <!-- /.Carousel -->

    <!--First Content -->
    <div class="kic-top">
        <div class="container">
            <div class="spec">
            <!-- <h3><a href="{{ url('featured-items')}}" class="no-link">Featured Items</a></h3> -->
                <h3 class="no-link">Featured Stores</h3>
                <div class="ser-t">
                    <b></b>
                    <span><i></i></span>
                    <b class="line"></b>
                </div>
            </div>
            <div class="row">
                @foreach($featured as $feature)
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="card">
                            <div class="featured-container">
                                <img src="{{ $feature->image }}" class="img-responsive image">
                                <div class="middle">
                                    <div class="text">
                                        {{ $feature->name }}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

        </div>
    </div>
    <!-- /.First Content-->

    <!-- Second Content -->
    <div class="product">
        <div class="container">
            <div class="spec">
                <h3><a href="{{url('food-items')}}" class="no-link">All Stores</a></h3>
                <div class="ser-t">
                    <b></b>
                    <span><i></i></span>
                    <b class="line"></b>
                </div>
            </div>

            <form action="/search" method="POST" role="search">
                {{ csrf_field() }}
                <div class="input-group">
                    <input type="text" class="form-control" name="q"
                           placeholder="Search users"> <span class="input-group-btn">
            <button type="submit" class="btn btn-default">
                <span class="glyphicon glyphicon-search"></span>
            </button>
        </span>
                </div>
            </form>
            <!--output search-->
            <div class="container">
                @if(isset($details))
                    <p> The Search results for your query <b> {{ $query }} </b> are :</p>
                    <h2>Sample User details</h2>
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($details as $user)
                            <tr>
                                <td>{{$products->name}}</td>
                                <td>{{$products->email}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                @endif
            </div>


            <div class="row">
                @foreach($foodstuffs as $foodstuff)
                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="card">
                            <a href="{{route('getitem', ['id' => $foodstuff->id])}}">
                                <img class="card-img-top"
                                     src="{{($foodstuff->image) ? $foodstuff->image : '/images/of27.png'}}"
                                     alt="Food Item">
                            </a>
                            <h4 class="card-title offset-2 item-name">{{$foodstuff->name}}</h4>
                            <div class="card-block">
                                <div class="badge-container" style="display: flow-root;">
                                    <div class="pull-left">
                                        <h6 class="item-info">Amount</h6>
                                        <div class="badges">
                                            &#8364; <span
                                                    id="{{$foodstuff->id}}">{{$foodstuff->dunit($foodstuff->id)->price}}</span>
                                        </div>
                                    </div>
                                    <div class="pull-right">
                                        <h6 class="item-info">Unit</h6>
                                        <?php $uid = 'unit' . $foodstuff->id ?>
                                        <select class="badges" onchange="changePrice(this.value)" id="{{$uid}}">
                                            @foreach($foodstuff->units as $unit)
                                                <option value="{{$unit->id}}">
                                                    {{$unit->type->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="add" style="margin-bottom: 20px;">
                                @if(Auth::check() && Auth::user()->role_id == 1)
                                    @if($foodstuff->is_featured == 0)
                                        <a href="{{url('feature-product/' .$foodstuff->id)}}">
                                            <button class="btn btn-danger my-cart-btn my-cart-b">Feature</button>
                                        </a>
                                    @else
                                        <a href="{{url('unfeature-product/' .$foodstuff->id)}}">
                                            <button class="btn btn-danger my-cart-btn my-cart-b">Unfeature</button>
                                        </a>
                                    @endif
                                @else
                                    <button id="idCart{{$foodstuff->id}}"
                                            class="btn btn-danger my-cart-btn my-cart-b"
                                            onclick="addToCart({{$foodstuff->id}})">Add to Cart
                                    </button>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
                <div id="pagination">
                    {{ $foodstuffs->links() }}
                </div>
            </div>


        </div>
    </div>
    <!-- /.Second Content -->

    <!-- Third Content -->
    <div class="product" style="margin-top: -100px;">
        <div class="container">
            <div class="spec">
                <h3><a href="{{url('provision-items')}}" class="no-link">Provision Items</a></h3>
                <div class="ser-t">
                    <b></b>
                    <span><i></i></span>
                    <b class="line"></b>
                </div>
            </div>

            <div class="row">
                @foreach($provisions as $provision)
                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="card">
                            <a href="{{route('getitem', ['id' => $provision->id])}}">
                                <img class="card-img-top"
                                     src="{{($provision->image) ? $provision->image : '/images/of27.png'}}"
                                     alt="Card image cap">
                            </a>
                            <h4 class="card-title offset-2 item-name">{{$provision->name}}</h4>
                            <div class="card-block">
                                <div class="badge-container" style="display: flow-root;">
                                    <div class="pull-left">
                                        <h6 class="item-info">Amount</h6>
                                        <div class="badges">
                                            &#8364; <span
                                                    id="{{$provision->id}}">{{$provision->dunit($provision->id)->price}}</span>
                                        </div>
                                    </div>
                                    <div class="pull-right">
                                        <h6 class="item-info">Unit</h6>
                                        <?php $uid = 'unit' . $provision->id ?>
                                        <select class="badges" onchange="changePrice(this.value)" id="{{$uid}}">
                                            @foreach($provision->units as $unit)
                                                <option value="{{$unit->id}}">
                                                    {{$unit->type->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="add" style="margin-bottom: 20px;">
                                @if(Auth::check() && Auth::user()->role_id == 1)
                                    @if($provision->is_featured == 0)
                                        <a href="{{url('feature-product/' .$provision->id)}}">
                                            <button class="btn btn-danger my-cart-btn my-cart-b">Feature</button>
                                        </a>
                                    @else
                                        <a href="{{url('unfeature-product/' .$provision->id)}}">
                                            <button class="btn btn-danger my-cart-btn my-cart-b">Unfeature</button>
                                        </a>
                                    @endif
                                @else
                                    <button id="idCart{{$provision->id}}" class="btn btn-danger my-cart-btn my-cart-b"
                                            onclick="addToCart({{$provision->id}})">Add to Cart
                                    </button>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
                <div id="pagination">
                    {{ $provisions->links() }}
                </div>
            </div>
        </div>
    </div>
    <!-- /.Third Content -->

    <script>
        function changePrice(id) {
            var baseUrl = "{{ url('/') }}";
            console.log('my id is' + id);
            $.get(baseUrl + '/item/' + id + '/unit', function (data) {
                console.log(data);
                //   $('#price' + data.product_id).text(data.price);
                console.log(data.product_id);
                document.getElementById(data.product_id).innerText = data.price;
            });
        }

        // When the user clicks on div, open the popup
        function myFunction() {
            var popup = document.getElementById("myPopup");
            popup.classList.toggle("show");
        }
    </script>
@stop
