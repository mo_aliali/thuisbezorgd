@extends('layouts.front.master')

@section('body')

    <!-- Banner -->
    <div class="banner-top">
        <div class="container">
            <h3>Provision Items</h3>
            <h4><a href="{{ url('/')}}">Home</a><label>/</label>Provision Items</h4>
            <div class="clearfix"></div>
        </div>
    </div>

    <!-- Second Content -->
    <div class="product">
        <div class="container">

            <div class="spec">
                <h3 class="green">Provision Items</h3>
                <div class="ser-t">
                    <b></b>
                    <span><i></i></span>
                    <b class="line"></b>
                </div>
            </div>

            <div class="row">
                @foreach($provisions as $provision)
                    <div class="col-lg-3 col-md-4 col-sm-6">
                        <div class="card">
                            <a href="#" data-toggle="modal" data-target="#myModal{{$provision->id}}">
                                <img class="card-img-top"
                                     src="{{($provision->image) ? $provision->image : '/images/of27.png'}}"
                                     alt="Provision Item">
                            </a>
                            <h4 class="card-title offset-2 item-name">{{$provision->name}}</h4>
                            <div class="card-block">
                                <div class="badge-container" style="display: flow-root;">
                                    <div class="pull-left">
                                        <h6 class="item-info">Amount</h6>
                                        <div class="badges">
                                            &#8364; <span
                                                    id="{{$provision->id}}">{{$provision->dunit($provision->id)->price}}</span>
                                        </div>
                                    </div>
                                    <div class="pull-right">
                                        <h6 class="item-info">Unit</h6>
                                        <?php $uid = 'unit' . $provision->id ?>
                                        <select class="badges" onchange="changePrice(this.value)" id="{{$uid}}">
                                            @foreach($provision->units as $unit)
                                                <option value="{{$unit->id}}">
                                                    {{$unit->type->name}}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="add" style="margin-bottom: 20px;">
                                @if(Auth::check() && Auth::user()->role_id == 1)
                                    @if($provision->is_featured == 0)
                                        <a href="{{url('feature-product/' .$provision->id)}}">
                                            <button class="btn btn-danger my-cart-btn my-cart-b">Feature</button>
                                        </a>
                                    @else
                                        <a href="{{url('unfeature-product/' .$provision->id)}}">
                                            <button class="btn btn-danger my-cart-btn my-cart-b">Unfeature</button>
                                        </a>
                                    @endif
                                @else
                                    <button class="btn btn-danger my-cart-btn my-cart-b"
                                            onclick="addToCart({{$provision->id}})">Add to Cart
                                    </button>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach


            </div>
            <!-- /.con-w3l agileinf -->

        </div>
    </div>
    @foreach($provisions as $provision)
        <div class="modal fade" id="myModal{{$provision->id}}" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content modal-info">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body modal-spa">
                        <div class="col-md-5 span-2">
                            <div class="item">
                                <img src="{{$provision->image}}" class="img-responsive" alt="">
                            </div>
                        </div>
                        <div class="col-md-7 span-1 ">
                            <h3>{{$provision->name}}</h3>
                            <p class="in-para">Provision</p>
                            <div class="price_single">
                                <span class="reducedfrom ">&#8364;<span>{{$provision->dunit($provision->id)->price}}</span></span>
                                <div class="clearfix"></div>
                            </div>
                            <h4 class="quick">Quick Overview:</h4>
                            <p class="quick_desc">{{$provision->description}}</p>
                            <div class="add-to">
                                <button id="idCart2{{$provision->id}}" class="btn btn-danger my-cart-btn my-cart-btn1"
                                        onclick="addToCart({{$provision->id}})">Add to Cart
                                </button>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    <script>
        function changePrice(id) {
            var baseUrl = "{{ url('/') }}";
            console.log('my id is' + id);
            $.get(baseUrl + '/item/' + id + '/unit', function (data) {
                console.log(data);
                //   $('#price' + data.product_id).text(data.price);
                console.log(data.product_id);
                document.getElementById(data.product_id).innerText = data.price;
            });
        }
    </script>
@stop
