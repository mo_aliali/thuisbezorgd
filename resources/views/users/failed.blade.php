@extends('layouts.front.master')
@section('body')

    <!-- Banner -->
    <div class="banner-top">
        <div class="container">
            <h3>Item</h3>
            <h4><a href="{{ url('/')}}">Home</a><label>/</label>Item</h4>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- ./Banner -->

    <div class="typrography" style="background-color: #f1f2f3;">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="card">

                        <div class="card-block">
                            <h3>Payment failed! Try to reinitaite payment. Thank You!</h3>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop
