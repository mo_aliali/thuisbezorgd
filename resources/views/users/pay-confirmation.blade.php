@extends('layouts.front.master')
@section('body')

    <!-- Banner -->
    <div class="banner-top">
        <div class="container">
            <h3>Confirmation</h3>
            <h4><a href="{{ url('/')}}">Home</a><label>/</label>Confirmation</h4>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- ./Banner -->

    <div class="typrography" style="background-color: #f1f2f3;">
        <div class="container">
            @if (isset($order))
                <div class="spec">
                    <h3>Confirmation</h3>
                    <div class="ser-t">
                        <b></b>
                        <span style="background-color: #f1f2f3;"><i></i></span>
                        <b class="line"></b>
                    </div>
                </div>

                <div class="confirmation">
                    <span class="good"></span>
                    <h2>Order Successful</h2>
                    @if($order->delivery_type == "paycard")
                        <h4>Your payment of &#8364;{{$order->total}} has been received. Your item will reach you
                            soon.</h4>
                    @elseif($order->delivery_type == "paycash")
                        <h4>Your order worth &#8364;{{$order->total}} has been confirmed. Your item will reach you soon.
                            You have opted to pay with cash make sure the above amount is available on delivery.</h4>
                    @else
                        <h4>Your order worth &#8364;{{$order->total}} has been confirmed. Your item will reach you soon.
                            You have opted to pay with POS make sure your ATM card is available on delivery.</h4>
                    @endif
                    <span class="ref">Reference : {{$order->order_ref}}</span>
                    <button class="goHomeBtn">
                        <a href="{{ url('/') }}" class="goHomeLink">Home</a>
                    </button>
                </div>

            @else
                <div class="alert alert-danger">sorry no order reference exists, you must have gotten here by error
                </div>
            @endif
        </div>
    </div>

@stop