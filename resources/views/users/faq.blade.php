@extends('layouts.front.master')
@section('body')

    <!--banner-->
    <div class="banner-top">
        <div class="container">
            <h3>Faqs</h3>
            <h4><a href="{{ url('/')}}">Home</a><label>/</label>Faqs</h4>
            <div class="clearfix"></div>
        </div>
    </div>
    <!-- /.banner -->

    <!-- faqs -->
    <div class="faq-w3 main-grid-border">
        <div class="container">
            <div class="spec">
                <h3>Faqs</h3>
                <div class="ser-t">
                    <b></b>
                    <span><i></i></span>
                    <b class="line"></b>
                </div>
            </div>
            <div class="panel-group" id="accordion">
                <!-- First Panel -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title" data-toggle="collapse" data-target="#collapseOne"><span>1. </span>What
                            is Super Market?</h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p>Super Market is the online platform that brings your market right to your door step</p>
                            <p>Your market purchases are all brought to your door step</p>
                        </div>
                    </div>
                </div>

                <!-- Second Panel -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title" data-toggle="collapse" data-target="#collapseTwo"><span>2. </span>How
                            will I be charged for my purchases?</h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p>Super Market takes care of taking down your market item purchases and charges for
                                bringing the items to your door step</p>
                        </div>
                    </div>
                </div>

                <!-- Third Panel -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title" data-toggle="collapse" data-target="#collapseThree"><span>3. </span>How
                            long will it take to get my purchased items?</h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p>Withing 2 - 3 hours after order have been confirmed all items will be delivered to your
                                door step</p>
                        </div>
                    </div>
                </div>

                <!-- Fourth Panel -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title" data-toggle="collapse" data-target="#collapseFour"><span>4. </span>How
                            will I make payment for items purchased?</h4>
                    </div>
                    <div id="collapseFour" class="panel-collapse collapse">
                        <div class="panel-body">
                            <p>Payment for delivered items can be made via the following options</p>
                            <ul>
                                <li><p>Secure online payment gateway system</p></li>
                                <li><p>Pay on delivery (cash or pos)</p></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- /.faq -->

@stop