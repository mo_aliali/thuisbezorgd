@extends('layouts.front.master')

@section('body')

    <!-- Banner -->
    <div class="banner-top">
        <div class="container">
            <h3>Login</h3>
            <h4><a href="#">Home</a><label>/</label>Login</h4>
            <div class="clearfix"></div>
        </div>
    </div>

    <!-- Login -->
    <!-- <div class="login">
		@if (Session::has('success'))
        <div class="">
            <div class="alert alert-success text-center"> {{ Session::get('success') }}</div>
			</div>
		@elseif (Session::has('fail'))
        <div class="">
            <div class="alert alert-danger text-center"> {{ Session::get('fail') }}</div>
			</div>
		@endif
            <div class="main-agileits">
                <div class="form-w3agile">
                    <h3>Login</h3>
                    <form action="{{ URL::route('postLogin')}}" method="post" enctype="multipart/form-data">
					<div class="key">
						<i class="fa fa-envelope" aria-hidden="true"></i>
						<input type="text" value="{{old('username') ? old('username') : 'username'}}" placeholder="username"  name="username" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Username';}" required="">
						<div class="clearfix"></div>
						@if ($errors->has('username'))
        <span style="color: palevioletred; margin-top: 10%;">{{ $errors->first('username') }}</span>
						@endif
            </div>
            <div class="key">
                <i class="fa fa-lock" aria-hidden="true"></i>
                <input type="password" value="Password" value="{{old('password') ? old('password') : 'password'}}" name="password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required="">
						<div class="clearfix"></div>
						@if ($errors->has('password'))
        <span style="color: palevioletred; margin-top: 10%;">{{ $errors->first('password') }}</span>
						@endif
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="submit" value="Login">
				</form>
			</div>
			<div class="forg">
				<a href="#" class="forg-left">Forgot Password?</a>
				<a href="{{ url('register')}}" class="forg-right">Register</a>
				<div class="clearfix"></div>
			</div>
		</div>
	</div> -->

    <div class="login">
        @if (Session::has('success'))
            <div class="">
                <div class="alert alert-success text-center"> {{ Session::get('success') }}</div>
            </div>
        @elseif (Session::has('fail'))
            <div class="">
                <div class="alert alert-danger text-center"> {{ Session::get('fail') }}</div>
            </div>
        @endif

        <div class="auth-container">
            <h3>Login</h3>
            <form action="{{ URL::route('postLogin')}}" method="post" enctype="multipart/form-data">
                <div class="authInput-container">
                    <label>Username</label>
                    <input type="text" placeholder="Enter Username" name="username" required="" class="input-field">
                </div>
                @if ($errors->has('username'))
                    <span style="color: palevioletred; margin-top: 10%;">
						{{ $errors->first('username') }}
					</span>
                @endif

                <div class="authInput-container">
                    <label>Password</label>
                    <input type="password" placeholder="Enter Password" name="password" required="" class="input-field">
                </div>
                @if ($errors->has('password'))
                    <span style="color: palevioletred; margin-top: 10%;">
						{{ $errors->first('password') }}
					</span>
                @endif

                <div class="forgot-password"><a href="{{ url('forgot-password') }}">Forgot Your Password</a></div>
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="button-container">
                    <a href="{{ url('register')}}" class="registerBtn">Register</a>
                    <input type="submit" value="Sign In" class="loginBtn">
                </div>
            </form>
        </div>
    </div>

    <!-- /.Login -->

@stop