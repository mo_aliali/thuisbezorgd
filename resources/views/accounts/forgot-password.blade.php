@extends('layouts.front.master')

@section('body')

    <!-- Banner -->
    <div class="banner-top">
        <div class="container">
            <h3>Forgot Password</h3>
            <h4><a href="#">Home</a><label>/</label>Forgot Password</h4>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="login">
        @if (Session::has('success'))
            <div class="">
                <div class="alert alert-success text-center"> {{ Session::get('success') }}</div>
            </div>
        @elseif (Session::has('fail'))
            <div class="">
                <div class="alert alert-danger text-center"> {{ Session::get('fail') }}</div>
            </div>
        @endif

        <div class="auth-container">
            <h3>Forgot Password</h3>
            <form method="post" enctype="multipart/form-data" action="{{route('password-resets')}}">
                <div class="authInput-container">
                    <label>Email</label>
                    <input type="email" placeholder="Enter Email" name="email" required="" class="input-field">
                </div>
                <div class="authInput-container">
                    <label>New Password</label>
                    <input type="password" placeholder="Enter Password" name="password" required="" class="input-field">
                </div>
                <div class="authInput-container">
                    <label>Confirm New Password</label>
                    <input type="password" placeholder="Confirm Password" name="password_confirmation" required=""
                           class="input-field">
                </div>

                <div class="forgotBtn-container">
                    <input type="submit" value="Submit" class="forgotBtn">
                </div>
            </form>
        </div>
    </div>

@stop